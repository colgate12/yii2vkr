<?php
return [
	'class'=>'yii\web\UrlManager',
	'enablePrettyUrl'=>true,
	'showScriptName'=>false,
	'rules'=>[
        '<controller:system>/<action:[\w\d-_/]+>' => '<controller>/<action>',

		'GET <controller:[\w-]+>' => '<controller>/index',
		'POST <controller:[\w-]+>' => '<controller>/create',
		'GET <controller:[\w-]+>/<id:\d+>' => '<controller>/view',
//		'GET <controller:[\w-]+>/<slug:[\w-]+>' => '<controller>/slug-view',
		'PUT <controller:[\w-]+>/<id:\d+>' => '<controller>/update',
		'DELETE <controller:[\w-]+>/<id:\d+>' => '<controller>/delete',

		'<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',
		'<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',
	]
];