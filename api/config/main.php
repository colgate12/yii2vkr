<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['session'],
    'language' => 'ru-RU',
    'controllerNamespace' => 'api\controllers',
    'defaultRoute' => 'site/index',
    'modules' => require(__DIR__.'/modules.php'),
    'components' => require(__DIR__.'/components.php'),
    'params' => $params,
];
