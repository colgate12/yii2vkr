<?php

return [
    'request' => [
        'enableCsrfValidation'=>false,
        'parsers' => [
            'application/json' => 'yii\web\JsonParser',
        ]
    ],
    'response' => [
        'format' => 'json'
    ],
    'user' => [
        'identityClass' => 'api\models\User',
        'enableAutoLogin' => true,
        'loginUrl' => null,
        'identityCookie' => [
            'name' => '_frontendUser',
            'path'=>'/frontend/web'
        ],
        'authTimeout' => 3600 * 24 * 30,
    ],
    'session' => [
        'name' => '_frontendSessionId',
        'timeout' => 3600 * 24 * 30,
        'cookieParams' => [
            'httponly' => true,
            'lifetime' => 3600 * 24 * 30
        ],
        'useCookies' => true,
    ],
    'authManager' => [
        'class' => 'yii\rbac\DbManager',
        'itemTable' => '{{%user_auth_item}}',
        'itemChildTable' => '{{%user_auth_item_child}}',
        'assignmentTable' => '{{%user_auth_assignment}}',
        'ruleTable' => '{{%user_auth_rule}}',
    ],
    'cache' => [
        'class' => 'yii\caching\FileCache',
        'cachePath' => '@frontend/runtime/cache'
    ],
    'errorHandler' => [
        'errorAction' => 'system/error',
    ],
    'urlManager' => require(__DIR__.'/urlManager.php'),
];