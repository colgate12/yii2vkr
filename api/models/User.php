<?php

namespace api\models;

use common\models\User as CommonUser;
use yii\helpers\ArrayHelper;


/**
 * Пользователи
 */
class User extends CommonUser
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $items = parent::behaviors();

        $items['upload']['instanceByName'] = true;

        return $items;
    }

    /**
     * @return array
     */
    public function fields()
    {
        $items = parent::fields();

        $items['image_link'] = 'imageUrl';
        $items['network_url'] = 'networkUrl';

        return $items;
    }

    /**
     * @return array
     */
    public function excludeFields()
    {
        return ArrayHelper::merge(parent::excludeFields(), [
            'password_hash', 'password_reset_token', 'auth_key', 'ip', 'network_key'
        ]);
    }

    /**
     * @return array
     */
    public function extraFields()
    {
        $items = parent::extraFields();

        $items['created_at'] = 'created';
        $items['updated_at'] = 'updated';

        return $items;
    }
}