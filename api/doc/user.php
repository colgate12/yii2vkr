<?php

/**
 * @api {get} /user Просмотр профиля
 * @apiSampleRequest /user
 * @apiName Index
 * @apiGroup User
 * @apiPermission Пользователям
 * @apiDescription Просмотр профиля пользователя.
 *
 * @apiSuccess (Атрибуты ответа) {Integer} id ID записи в базе
 * @apiSuccess (Атрибуты ответа) {String} network_id ID в соц. сети
 * @apiSuccess (Атрибуты ответа) {String} email E-mail
 * @apiSuccess (Атрибуты ответа) {String} phone Телефон
 * @apiSuccess (Атрибуты ответа) {String} image_link Ссылка на фотографию
 * @apiSuccess (Атрибуты ответа) {String} first_name Имя
 * @apiSuccess (Атрибуты ответа) {String} last_name Фамилия
 * @apiSuccess (Атрибуты ответа) {String} middle_name Отчество
 * @apiSuccess (Атрибуты ответа) {Integer=0=На_модерации,1=Активен,2=Неактивен,3=На_удалении} status_key Статус
 * @apiSuccess (Атрибуты ответа) {String} created_at Дата создания
 * @apiSuccess (Атрибуты ответа) {String} updated_at Дата изменения
 *
 * @apiUse Error
 *
 * @apiSuccessExample {json} По умолчанию:
 *      HTTP/1.1 200 OK
 *      {
 *          "id": 2
 *          "network_id": null
 *          "email": "asdixer+1@gmail.com"
 *          "image_link": null
 *          "first_name": "Vlad"
 *          "last_name": "Platonov"
 *          "status_key": 1
 *          "created_at": "2015-12-24 18:17:29"
 *          "updated_at": "2015-12-24 18:17:29"
 *      }
 *
 * @apiUse ErrorNotAllowedGet
 */

/**
 * @api {put} /user Изменение профиля
 * @apiSampleRequest /user
 * @apiName Update
 * @apiGroup User
 * @apiPermission Пользователям
 * @apiDescription Изменение профиля пользователя.
 *
 * @apiParam (Атрибуты запроса) {String} first_name Имя
 * @apiParam (Атрибуты запроса) {String} last_name Фамилия
 * @apiParam (Атрибуты запроса) {String} middle_name Отчество
 * @apiParam (Атрибуты запроса) {String} phone Телефон
 * @apiParam (Атрибуты запроса) {String} email E-mail
 * @apiParam (Атрибуты запроса) {File} file_image_link Файл аватара для загрузки
 *
 * @apiSuccess (Атрибуты ответа) {Integer} id ID записи в базе
 * @apiSuccess (Атрибуты ответа) {String} network_id ID в соц. сети
 * @apiSuccess (Атрибуты ответа) {String} email E-mail
 * @apiSuccess (Атрибуты ответа) {String} phone Телефон
 * @apiSuccess (Атрибуты ответа) {String} image_link Ссылка на фотографию
 * @apiSuccess (Атрибуты ответа) {String} first_name Имя
 * @apiSuccess (Атрибуты ответа) {String} last_name Фамилия
 * @apiSuccess (Атрибуты ответа) {String} middle_name Отчество
 * @apiSuccess (Атрибуты ответа) {Integer=0=На_модерации,1=Активен,2=Неактивен,3=На_удалении} status_key Статус
 * @apiSuccess (Атрибуты ответа) {String} created_at Дата создания
 * @apiSuccess (Атрибуты ответа) {String} updated_at Дата изменения
 *
 * @apiUse Error
 *
 * @apiSuccessExample {json} По умолчанию:
 *      HTTP/1.1 200 OK
 *      {
 *          "id": 2
 *          "network_id": null
 *          "email": "asdixer+1@gmail.com"
 *          "image_link": null
 *          "first_name": "Vlad1"
 *          "last_name": "Platonov"
 *          "status_key": 1
 *          "created_at": "2015-12-24 18:17:29"
 *          "updated_at": "2015-12-24 18:41:50"
 *      }
 *
 * @apiErrorExample {json} Ошибки валидации:
 *      HTTP/1.1 422 Data Validation Failed
 *      {
 *          0: {
 *              field: "email"
 *              message: "Значение «E-mail» не является правильным email адресом."
 *          }
 *      }
 *
 * @apiUse ErrorNotAllowedPut
 */