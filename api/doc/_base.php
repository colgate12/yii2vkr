<?php
/**
 * @apiDefine IndexDefaultParams
 * @apiParam (Атрибуты запроса GET) {Integer} limit=10 Количество записей для выдачи
 * @apiParam (Атрибуты запроса GET) {Integer} offset=0 С какой записи начинать
 * @apiParam (Атрибуты запроса GET) {String} [sort] Как сортировать выборку (<u>Поля через запятую</u>, <code>attribute</code> - по возрастанию, <code>-attribute</code> - по убыванию)
 * @apiParam (Атрибуты запроса GET) {String} [fields="Все"] Какие поля выбирать (<u>Через запятую</u>)
 * @apiParam (Атрибуты запроса GET) {Object} [where] Конструктор запросов (<a href="#api-_QueryBuilder-Index">Документация</a>)
 */

/**
 * @apiDefine IndexDefaultSuccess
 * @apiSuccess (Атрибуты ответа) {Integer} offset С какой записи начинать
 * @apiSuccess (Атрибуты ответа) {Integer} limit Количество записей для выдачи
 * @apiSuccess (Атрибуты ответа) {Integer} count_all Количество всех записей
 * @apiSuccess (Атрибуты ответа) {Integer} count_current Количество текущей выдачи
 */

/**
 * @apiDefine Error
 * @apiError (Атрибуты ошибки) {String} name Название ошибки
 * @apiError (Атрибуты ошибки) {String} message Описание ошибки
 * @apiError (Атрибуты ошибки) {Integer=0} code Служебная информация
 * @apiError (Атрибуты ошибки) {Integer=302(Редирект),400(Плохой_запрос),401(Не_авторизован),403(Запрещено),404(Не_найдено),405(Метод_не_разрешен),422(Ошибка_валидации),500(Ошибка_сервера)} status Статус ответа
 * @apiError (Атрибуты ошибки) {String} type Тип исключения
 */

/**
 * @apiDefine ErrorNotAllowedGet
 * @apiErrorExample {json} Метод не разрешен:
 *      HTTP/1.1 405 Method Not Allowed
 *      {
 *          "name": "Method Not Allowed"
 *          "message": "Method Not Allowed. This url can only handle the following request methods: GET."
 *          "code": 0
 *          "status": 405
 *          "type": "yii\web\MethodNotAllowedHttpException"
 *      }
 */

/**
 * @apiDefine ErrorNotAllowedPost
 * @apiErrorExample {json} Метод не разрешен:
 *      HTTP/1.1 405 Method Not Allowed
 *      {
 *          "name": "Method Not Allowed"
 *          "message": "Method Not Allowed. This url can only handle the following request methods: POST."
 *          "code": 0
 *          "status": 405
 *          "type": "yii\web\MethodNotAllowedHttpException"
 *      }
 */

/**
 * @apiDefine ErrorNotAllowedPut
 * @apiErrorExample {json} Метод не разрешен:
 *      HTTP/1.1 405 Method Not Allowed
 *      {
 *          "name": "Method Not Allowed"
 *          "message": "Method Not Allowed. This url can only handle the following request methods: PUT."
 *          "code": 0
 *          "status": 405
 *          "type": "yii\web\MethodNotAllowedHttpException"
 *      }
 */

/**
 * @apiDefine ErrorNotAllowedDelete
 * @apiErrorExample {json} Метод не разрешен:
 *      HTTP/1.1 405 Method Not Allowed
 *      {
 *          "name": "Method Not Allowed"
 *          "message": "Method Not Allowed. This url can only handle the following request methods: DELETE."
 *          "code": 0
 *          "status": 405
 *          "type": "yii\web\MethodNotAllowedHttpException"
 *      }
 */

/**
 * @apiDefine ErrorNotFound
 * @apiErrorExample {json} Не найдено:
 *      HTTP/1.1 404 Not Found
 *      {
 *          "name": "Not found"
 *          "message": "Not found"
 *          "code": 0
 *          "status": 404
 *          "type": "yii\web\NotFoundHttpException"
 *      }
 */

/**
 * @apiDefine ErrorValidationFailed
 * @apiErrorExample {json} Ошибки валидации:
 *      HTTP/1.1 422 Data Validation Failed
 *      {
 *          0: {
 *              "field": "name"
 *              "message": "Необходимо заполнить «Название»."
 *          }
 *      }
 */