<?php
/**
 * @api {post} /execute/pack Массовое выполнение запросов
 * @apiSampleRequest /execute/pack
 * @apiName Pack
 * @apiGroup Execute
 * @apiDescription Позволяет одним запросом вызывать несколько методов апи.
 *
 * @apiParamExample {json} Пример JSON:
 *      {
 *          "category/index": [
 *              {
 *                  "GET": {
 *                      "offset": 0,
 *                      "limit": 10,
 *                  }
 *              }
 *          ]
 *      }
 */