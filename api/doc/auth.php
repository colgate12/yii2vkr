<?php
/**
 * @api {post} /auth/logout Выход
 * @apiSampleRequest /auth/logout
 * @apiName Logout
 * @apiGroup Auth
 * @apiPermission Пользователям
 * @apiDescription Закрытие сессии текущего пользователя.
 *
 * @apiUse Error
 *
 * @apiSuccessExample {json} По умолчанию:
 *      HTTP/1.1 200 OK
 *      {
 *          "true"
 *      }
 *
 * @apiUse ErrorNotAllowedPost
 */

/**
 * @api {get} /auth/social-url Ссылки для авторизации через соц. сети
 * @apiSampleRequest /auth/social-url
 * @apiName SocialUrl
 * @apiGroup Auth
 * @apiPermission Гостям
 * @apiDescription Возвращает возможный список ссылок для авторизации через соц. сети.
 *
 * @apiParam (Атрибуты запроса GET) {Integer=1)} [absolute] Вернуть абсолютные ссылки вместо относительных
 *
 * @apiUse Error
 *
 * @apiSuccessExample {json} По умолчанию:
 *      HTTP/1.1 200 OK
 *      {
 *          "vkontakte": "/auth/social/vkontakte"
 *          "facebook": "/auth/social/facebook"
 *          "odnoklassniki": "/auth/social/odnoklassniki"
 *          "instagram": "/auth/social/instagram"
 *          "twitter": "/auth/social/twitter"
 *      }
 *
 * @apiUse ErrorNotAllowedGet
 */