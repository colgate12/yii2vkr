<?php
/**
 * @api {get} ?where Куонструктор запросов
 * @apiSampleRequest off
 * @apiName Index
 * @apiGroup _QueryBuilder
 * @apiDescription Конструктор запросов позволяет добавлять гибкие условия в выборку.
 *
 * @apiExample {get} Array:
 *      http://dev-.devup.cc/api/example?where[id][in]=1,2,3
 *
 * @apiExample {get} Json:
 *      http://dev-.devup.cc/api/example?where={"id":{"in":[1,2,3]}}
 *
 * @apiParam (Атрибуты запроса) {String} parameter Атрибут сущности для фильтрации
 * @apiParam (Атрибуты запроса) {String} operator Опуратор фильтрации
 * @apiParam (Атрибуты запроса) {String} operator.eq Атрибут равен значению (<u>только одно значение</u>)
 * @apiParam (Атрибуты запроса) {String} operator.neq Атрибут не равен значению (<u>только одно значение</u>)
 * @apiParam (Атрибуты запроса) {String} operator.like Значение является подстрокой атрибута (<u>только одно значение</u>)
 * @apiParam (Атрибуты запроса) {String} operator.nlike Значение не является подстрокой атрибута (<u>только одно значение</u>)
 * @apiParam (Атрибуты запроса) {String} operator.in Атрибут входит в множество значений (<u>множество значений</u>)
 * @apiParam (Атрибуты запроса) {String} operator.nin Атрибут не входит в множество значений (<u>множество значений</u>)
 * @apiParam (Атрибуты запроса) {String} operator.gt Атрибут больше значения (<u>только одно значение</u>)
 * @apiParam (Атрибуты запроса) {String} operator.gteq Атрибут больше либо равен значению (<u>только одно значение</u>)
 * @apiParam (Атрибуты запроса) {String} operator.lt Атрибут меньше значения (<u>только одно значение</u>)
 * @apiParam (Атрибуты запроса) {String} operator.lteq Атрибут меньше либо равен значению (<u>только одно значение</u>)
 * @apiParam (Атрибуты запроса) {Variable} value Значение для оператора (одно значение либо множество значений в <u>массиве или через запятую</u>)
 *
 * @apiParamExample {json} Пример JSON:
 *      {
 *          "count_like": {
 *              "neq": 15
 *              "gt": 10
 *              "lteq": 50
 *          }
 *          "name": {
 *              "like": "Москва"
 *              "nlike": "выделенных"
 *          }
 *          "id": {
 *              "in": [
 *                  1, 2, 5
 *              ]
 *              "nin": [
 *                  3, 4, 7
 *              ]
 *          }
 *      }
 *
 * @apiParamExample {Array} Пример ARRAY:
 *      [
 *          "count_like": [
 *              "neq": 15
 *              "gt": 10
 *              "lteq": 50
 *          ]
 *          "name": [
 *              "like": "Москва"
 *              "nlike": "выделенных"
 *          ]
 *          "id": [
 *              "in": [
 *                  1, 2, 5
 *              ]
 *              "nin": [
 *                  3, 4, 7
 *              ]
 *          ]
 *      ]
 */