<?php


namespace api\db;


use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class ApiQuery
 * @package api\db
 */
class ApiQuery extends \platx\rest\ApiQuery
{
    /**
     * @var array Query default parameters
     */
    public $defaultParams = [];

    /**
     * @param $params
     * @param array $defaultParams
     *
     * @return $this
     */
    public function set($params, $defaultParams = [])
    {
        if (!empty($params) && !is_array($params)) {
            $params = Json::decode($params);
        }

        if(!empty($defaultParams) && is_array($defaultParams)) {
            $this->defaultParams = $defaultParams;
        }

        $this->params = ArrayHelper::merge($this->defaultParams, (array)$params);

        return $this;
    }
}