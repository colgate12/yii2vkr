<?php

namespace api\controllers;

use api\base\controllers\Controller;
use api\models\User;
use platx\httperror\HttpError;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;


/**
 * Пользователи
 * @package api\controllers
 */
class UserController extends Controller
{
    /**
     * @var string Модель с которой работает контроллер
     */
    public $modelClass = 'api\models\User';

    /**
     * @var string Форма фильтрации с которой работает контроллер
     */
    public $searchFormClass = false;

    /**
     * Подключение общих поведений
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'update'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return User
     */
    public function actionIndex()
    {
        /** @var User $model */
        $model = new $this->modelClass();

        $model = $model->findOne(Yii::$app->user->id);

        return $model;
    }

    /**
     * @return User
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate()
    {
        /** @var User $model */
        $model = new $this->modelClass();

        $model = $model->findOne(Yii::$app->user->id);

        if (empty($model)) {
            HttpError::the404('Not found');
        }
        $model->setScenario('update');

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $model;
    }
}