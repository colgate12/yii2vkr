<?php

namespace api\forms;

use api\base\forms\SearchForm;


/**
 * Пользователи
 * @package api\forms
 */
class UserSearchForm extends SearchForm
{
    /**
     * @var string
     */
    public $modelClass = '\api\models\User';
}