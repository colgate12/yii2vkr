<?php

namespace api\pack;

use common\db\ActiveRecord;
use platx\httperror\HttpError;
use platx\rest\actions\Action;
use platx\rest\Controller;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;


/**
 * Class RequestHandler
 * @package api\pack
 */
class RequestHandler extends Model
{
    /**
     * @var array
     */
    public $allowedRoutes = [
        'user' => [
            'index', 'update'
        ],
    ];

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function parseRequest()
    {
        $responseHandler = new ResponseHandler();

        $params = Yii::$app->request->getBodyParams();
        
        if(!empty($params)) {
            foreach ($params as $method => $methodParamsArray) {
                $parsedMethod = $this->parseMethod($method);

                foreach ($methodParamsArray as $key => $methodParams) {
                    $get = ArrayHelper::getValue($methodParams, 'GET', []);
                    $post = ArrayHelper::getValue($methodParams, 'POST', []);

                    $result = $this->runAction($parsedMethod['controller'], $parsedMethod['action'], $get, $post);

                    if ($result instanceof ActiveRecord) {
                        $fields = ArrayHelper::getValue($get, 'fields', '');;
                        $expand = ArrayHelper::getValue($get, 'expand', '');

                        $fields = preg_split('/\s*,\s*/', $fields, -1, PREG_SPLIT_NO_EMPTY);
                        $expand = preg_split('/\s*,\s*/', $expand, -1, PREG_SPLIT_NO_EMPTY);

                        $responseHandler->response[$method][$key] = $result->toArray($fields, $expand);

                        if($result->hasErrors()) {
                            Yii::$app->getResponse()->setStatusCode(422, 'Data Validation Failed');
                            $responseHandler->response[$method][$key]['errors'] = $result->getErrors();
                        }
                    } else {
                        $responseHandler->response[$method][$key] = $result;
                    }
                }
            }
        }

        return $responseHandler->response;
    }

    /**
     * @param $rawMethod
     *
     * @return array
     * @throws \yii\web\BadRequestHttpException
     */
    public function parseMethod($rawMethod)
    {
        $parsedMethod = explode('/', $rawMethod);

        if (count($parsedMethod) > 2) {
            HttpError::the400("Method '{$rawMethod}' is wrong!");
        }

        $controller = ArrayHelper::getValue($parsedMethod, 0);
        $action = ArrayHelper::getValue($parsedMethod, 1, 'index');

        $this->checkAccess($controller, $action);

        return [
            'controller' => $controller,
            'action' => $action,
        ];
    }

    /**
     * @param $controller
     * @param $action
     * @param $getParams
     * @param $postParams
     *
     * @return bool|mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function runAction($controller, $action, $getParams = [], $postParams = []) {
        /** @var Controller[] $controller */
        $controller = Yii::$app->createController($controller);

        /** @var Action $action */
        $action = $controller[0]->createAction($action);

        Yii::$app->request->getMethod();

//        if($controller[0]->getBehavior('access')->beforeAction($action)) {
            Yii::$app->request->setBodyParams($postParams);
            Yii::$app->request->setQueryParams($getParams);

            return $action->runWithParams($getParams);
//        }

//        return false;
    }

    /**
     * @param $controller
     * @param $action
     *
     * @throws \yii\web\ForbiddenHttpException
     */
    public function checkAccess($controller, $action)
    {
        foreach ($this->allowedRoutes as $key => $allowedRoute) {
            if(is_string($allowedRoute)) {
                $explodedRoute = explode('/', $allowedRoute);

                if(count($explodedRoute) > 1) {
                    $this->allowedRoutes[$explodedRoute[0]][] = $explodedRoute[1];
                } else {
                    $this->allowedRoutes[] = $explodedRoute[0];
                }
            }
        }

        if ((!in_array($controller, $this->allowedRoutes)) && (!ArrayHelper::getValue($this->allowedRoutes, $controller) || !in_array($action, $this->allowedRoutes[$controller]))) {
            HttpError::the403("Route {$controller}/{$action} is not allowed!");
        }
    }
}