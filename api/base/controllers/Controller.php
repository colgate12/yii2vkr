<?php

namespace api\base\controllers;

use common\helpers\HttpError;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class Controller
 */
abstract class Controller extends \yii\rest\Controller
{
    /**
     * @var string
     */
    public $modelClass;

    /**
     * @var string
     */
    public $searchFormClass;

    /**
     * @throws \yii\web\HttpException
     */
    public function init()
    {
        if ($this->modelClass === null) {
            throw new InvalidConfigException(get_class($this) . '::$modelClass must be set.');
        }

        if ($this->searchFormClass === null) {
            throw new InvalidConfigException(get_class($this) . '::$searchFormClass must be set.');
        }

        parent::init();
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats'] = ['application/json' => Response::FORMAT_JSON];
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => $this->accessRules()
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function verbs()
    {
        return [
            'create' => ['post'],
            'update' => ['put', 'post', 'update'],
            'delete' => ['post', 'delete'],
            'index' => ['get'],
            'view' => ['get'],
        ];
    }

    /**
     * Access rules for controller actions
     *
     * @return array
     */
    public function accessRules()
    {
        return [];
    }

    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (false === Yii::$app->request->getIsAjax()) {
            HttpError::the400('Session Expired');
        }

        if (YII_ENV === 'dev') {
            Yii::$app->response->getHeaders()->set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
            Yii::$app->response->getHeaders()->set('Access-Control-Allow-Origin', 'http://localhost:8080');
        }

        return parent::beforeAction($action);
    }

}