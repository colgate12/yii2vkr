<?php

namespace api\base\actions;

use common\db\ActiveRecord;
use platx\httperror\HttpError;
use Yii;
use yii\base\Action;


/**
 * Class SlugViewAction
 * @package api\base\actions
 */
class SlugViewAction extends Action
{
    /**
     * @var string
     */
    public $modelClass;

    /**
     * @throws \yii\web\HttpException
     */
    public function init()
    {
        if (!$this->modelClass) {
            HttpError::the500('$modelClass property must be set!');
        }

        parent::init();
    }

    /**]
     * @param $slug
     * @return ActiveRecord
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($slug)
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass();

        $model = $model->findOne(['slug' => $slug]);

        if (empty($model)) {
            HttpError::the404('Not found');
        }

        return $model;
    }
}