<?php

namespace api\base\forms;

use api\db\ApiQuery;
use common\db\ActiveRecord;
use platx\rest\SearchForm as PlatxSearchForm;


/**
 * Class SearchForm
 * @package api\base\forms
 */
class SearchForm extends PlatxSearchForm
{
    /**
     * @return array
     */
    public function defaultParams()
    {
        return [];
    }

    /**
     * @param $query
     * @param $params
     * @return null|\yii\db\Query|\yii\db\ActiveQuery
     */
    public function setWhere($query, $params)
    {
        return (new ApiQuery())->set($params, $this->defaultParams())->build($query);
    }

    /**
     * @param array $attributes
     * @return \yii\db\ActiveQuery
     */
    public function buildQuery($attributes = [])
    {
        $this->setAttributes($attributes);

        if(!empty($this->modelClass) && empty($this->query)) {
            /** @var ActiveRecord $model */
            $model = new $this->modelClass();
            $this->query = $model->find();
        }

        if($this->validate()) {
            if (!empty($this->query)) {
                $this->filterAttributes();
            }

            if (($sort = $this->getSort()) !== false) {
                $this->query->addOrderBy($sort->getOrders());
            }

            if ($this->whereEnabled && ($this->where || $this->defaultParams())) {
                $this->query = $this->setWhere($this->query, $this->where);
            }

            return $this->query;
        }

        return null;
    }
}