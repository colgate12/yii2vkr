<?php

$config = [
    'homeUrl' => '/',
    'components' => [
        'request' => [
            'baseUrl' => '',
            'cookieValidationKey' => '',
        ],
        'authClientCollection' => require(__DIR__ . '/auth-clients-local.php'),
        'assetManager' => [
            'appendTimestamp' => true,
            'linkAssets' => true,
        ],
    ],
];

return $config;
