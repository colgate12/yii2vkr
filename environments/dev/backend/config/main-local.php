<?php

$baseUrl = '/adminx1';

$config = [
    'homeUrl' => $baseUrl,
    'components' => [
        'request' => [
            'baseUrl' => $baseUrl,
            'cookieValidationKey' => '',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'linkAssets' => true,
        ],
    ],
];

$config['bootstrap'][] = 'gii';
$config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
    'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '91.210.119.111'],
    'generators' => [
        'model' => 'common\gii\generators\model\Generator',
        'crud' => 'common\gii\generators\crud\Generator',
        'migration' => 'common\gii\generators\migration\Generator',
    ],
];

return $config;
