<?php

$config = [
    'components' => [
        'db' => require(__DIR__ . '/db-local.php'),
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
];

$logDate = date('d.m.Y');

$config['bootstrap'][] = 'log';
$config['components']['log'] = [
    'traceLevel' => 3,
    'targets' => [
        [
            'class' => 'yii\log\FileTarget',
            'levels' => ['error'],
            'logFile' => "@runtime/logs/error_{$logDate}.log",
            'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
            'except' => [
                'yii\web\HttpException:404',
            ],
        ],
        [
            'class' => 'yii\log\FileTarget',
            'levels' => ['warning'],
            'logFile' => "@runtime/logs/warning_{$logDate}.log",
            'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
            'except' => [
                'yii\web\HttpException:404',
            ],
        ],
        [
            'class' => 'yii\log\FileTarget',
            'levels' => ['info'],
            'logFile' => "@runtime/logs/info_{$logDate}.log",
            'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION'],
        ],
        [
            'class' => 'yii\log\FileTarget',
            'levels' => ['trace'],
            'logFile' => "@runtime/logs/trace_{$logDate}.log",
            'logVars' => ['_GET', '_POST', '_FILES'],
        ],
    ],
];


// configuration adjustments for 'dev' environment
$config['bootstrap'][] = 'debug';
$config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '91.210.119.111'],
];

return $config;
