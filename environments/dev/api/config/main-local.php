<?php

$baseUrl = '/api';

$config = [
    'homeUrl' => $baseUrl,
    'components' => [
        'request' => [
            'baseUrl' => $baseUrl,
            'cookieValidationKey' => '',
        ],
    ],
];

return $config;
