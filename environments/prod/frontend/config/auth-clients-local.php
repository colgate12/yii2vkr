<?php

return [
	'class' => 'yii\authclient\Collection',
	'clients' => [
		'vkontakte' => [
			'class' => 'frontend\components\authclients\Vkontakte',
			'clientId' => '',
			'clientSecret' => '',
			'title' => 'ВКонтакте',
			'scope' => ['email'],
			'attributeNames' => [
				'photo_100', 'sex', 'bdate', 'city', 'email', 'nickname', 'maiden_name', 'screen_name', 'contacts'
			],
			'normalizeUserAttributeMap' => [
				'id' => 'uid',
				'gender'     => 'sex',
				'birthday'   => 'bdate',
				'middle_name'   => 'nickname',
				'phone' => 'home_phone',
				'avatar_url' => function ($attributes) {
					return isset($attributes['photo_100']) && $attributes['photo_100'] == 'http://vk.com/images/camera_100.png' ? null : $attributes['photo_100'];
				}
			],
		],
		'facebook' => [
			'class' => 'yii\authclient\clients\Facebook',
			'clientId' => '',
			'clientSecret' => '',
			'title' => 'Facebook',
			'scope' => 'user_posts',
			'attributeNames' => [
				'id', 'email', 'first_name', 'last_name', 'picture', 'birthday',
			],
			'normalizeUserAttributeMap' => [
				'uid'         => 'id',
				'birthday'   => 'birthday',
				'avatar_url' => function ($attributes) {
					$result = file_get_contents("http://graph.facebook.com/{$attributes['id']}/picture?type=large&redirect=0");

					$result = json_decode($result, true);

					if(isset($result['data']['is_silhouette']) && !$result['data']['is_silhouette'] && isset($result['data']['url'])) {
						return $result['data']['url'];
					}

					return null;
				}
			],
		],
        'odnoklassniki' => [
            'class' => 'frontend\components\authclients\Odnoklassniki',
            'clientId'     => '',
            'clientSecret' => '',
            'applicationKey' => '',
            'title'        => 'Одноклассники',
            'normalizeUserAttributeMap' => [
                'id' => 'uid',
                'birthday' => 'birthday',
                'avatar_url' => function ($attributes) {
                    if (!isset($attributes['pic_2']) || $attributes['pic_2'] == 'https://usd1.mycdn.me/res/stub_128x96.gif') {
                        return null;
                    }

                    return $attributes['pic_2'];
                }
            ],
        ],
		'instagram' => [
			'class' => 'frontend\components\authclients\Instagram',
			'clientId' => '',
			'clientSecret' => '',
			'normalizeUserAttributeMap' => [
				'avatar_url' => function ($attributes) {
					return isset($attributes['profile_picture']) ? $attributes['profile_picture'] : null;
				},
				'first_name'   => function($attributes) {
					$name = isset($attributes['full_name']) ? explode(' ', $attributes['full_name']) : null;
					return isset($name[0]) ? $name[0] : null;
				},
				'last_name'   => function($attributes) {
					$name = isset($attributes['full_name']) ? explode(' ', $attributes['full_name']) : null;
					return isset($name[1]) ? $name[1] : null;
				},
				'middle_name'   => 'username',
			],
		],
	],
];
