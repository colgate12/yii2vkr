<?php
return [
    'uploads' => [
        'size_list' => [
            '50x50',
            '50x0',
            '0x50',

            '100x100',
            '100x0',
            '0x100',

            '150x150',
            '150x0',
            '0x150',

            '200x200',
            '200x0',
            '0x200',

            '300x300',
            '300x0',
            '0x300',

            '400x400',
            '400x0',
            '0x400',

            '600x600',
            '600x0',
            '0x600',
        ],
    ],
];
