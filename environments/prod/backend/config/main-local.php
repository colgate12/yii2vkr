<?php
$baseUrl = '/adminx1';

$config = [
    'homeUrl' => $baseUrl,
    'components' => [
        'request' => [
            'baseUrl' => $baseUrl,
            'cookieValidationKey' => '',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'linkAssets' => false,
        ],
    ],
];

return $config;
