<?php

namespace console\controllers;

use backend\models\Admin;
use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;


/**
 * Class InstallController
 * @package console\controllers
 */
class InstallController extends Controller
{
    /**
     * @var string Username
     */
    public $email = 'admin@admin.ad';

    /**
     * @var string Username
     */
    public $username = 'admin';

    /**
     * @var string Пароль
     */
    public $password = 'qwerty';

    /**
     * @param string $actionId
     * @return array
     */
    public function options($actionId)
    {
        return ArrayHelper::merge(parent::options($actionId), [
            'username', 'email', 'password'
        ]);
    }

    /**
     *
     */
    public function actionIndex()
    {
        $this->actionRbac();
        $this->actionAdmin();
    }

    /**
     * Генерация ролей для админов
     */
    public function actionRbac()
    {
        $authManager = Yii::$app->authManager;

        $this->stdout('Clear admin rbac data... ');
        $authManager->removeAll();
        $this->stdout('OK' . PHP_EOL, Console::FG_GREEN);

        /**
         * ROLES
         */
        $this->stdout('Generate admin roles... ');

        $roleModer = $authManager->createRole('moder');
        $authManager->add($roleModer);

        $roleAdmin = $authManager->createRole('admin');
        $authManager->add($roleAdmin);

        $this->stdout('OK' . PHP_EOL, Console::FG_GREEN);

        $this->stdout('Generate admin inheritance rights... ');

        $authManager->addChild($roleAdmin, $roleModer);

        $this->stdout('End.' . PHP_EOL, Console::FG_GREEN);
    }

    /**
     * Регистрация админа
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionAdmin()
    {
        $this->stdout('Create admin... ');

        $model = new Admin();
        $model->username = $this->username;
        $model->email = $this->email;
        $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        $model->password_reset_token = Yii::$app->getSecurity()->generateRandomString();
        $model->generateAuthKey();

        if ($model->save()) {
            $auth = Yii::$app->authManager;
            $adminRole = $auth->getRole('admin');
            $auth->assign($adminRole, $model->primaryKey);

            $this->stdout('OK' . PHP_EOL, Console::FG_GREEN);
            $this->stdout("Username: {$this->username}" . PHP_EOL, Console::FG_YELLOW);
            $this->stdout("Password: {$this->password}" . PHP_EOL, Console::FG_YELLOW);
        } else {
            $this->stdout('ERROR' . PHP_EOL, Console::FG_RED);
            print_r($model->getErrors());
            print_r($model->getAttributes());
        }
    }
}