<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;


/**
 * Документация к АПИ
 * @package console\controllers
 */
class DocController extends Controller
{
    /**
     * @var string $input Откуда
     */
    public $input = '@api/doc/';

    /**
     * @var string $output Куда
     */
    public $output = '@api/web/doc/';

    /**
     * @param string $actionID
     *
     * @return array
     */
    public function options($actionID)
    {
        return ArrayHelper::merge(parent::options($actionID), [
            'from', 'to',
        ]);
    }

    /**
     * Генерация документации
     */
    public function actionGenerate()
    {
        $inputPath = Yii::getAlias($this->input);
        $outputPath = Yii::getAlias($this->output);

        exec("apidoc -i {$inputPath} -o $outputPath", $result);

        \yii\helpers\VarDumper::dump($result, 5, 0);
    }
}