<?php

namespace console\controllers;

use ImageOptimizer\OptimizerFactory;
use platx\filehelper\FileHelper;
use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;


/**
 * Class FixController
 * @package console\controllers
 */
class FixController extends Controller
{
    /**
     *
     */
    public function actionImage()
    {
        $this->stdout("Start image optimizing... \n", Console::FG_YELLOW);

        $files = FileHelper::findFiles(Yii::getAlias("@frontend/web/uploads/"));

        foreach ($files as $key => $file) {
            $this->stdout("File {$file}...");
            $this->_optimize($file);
            $this->stdout(" OK! \n", Console::FG_GREEN);
        }

        $this->stdout("Done! \n", Console::FG_YELLOW);
    }

    /**
     * @param $path
     */
    private function _optimize($path)
    {
        $factory = new OptimizerFactory();

        $info = pathinfo($path);

        $extension = ArrayHelper::getValue($info, 'extension');

        if(in_array($extension, ['jpg', 'png'])) {
            if ($extension == 'jpg') {
                $optimizer = $factory->get('jpegoptim');
            } else if($extension == 'png') {
                $optimizer = $factory->get('optipng');
            } else {
                $optimizer = $factory->get();
            }

            $optimizer->optimize($path);
        }
    }
}