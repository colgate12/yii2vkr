<?php

namespace console\controllers;

use console\base\controllers\Controller;
use Faker\Factory;
use Faker\Generator;
use yii\helpers\ArrayHelper;


/**
 * Генерация контента для теста
 * @package console\controllers
 */
class DemoDataController extends Controller
{
    /** @var null|Generator $_faker */
    public $_faker;

    /**
     * @var int
     */
    public $count = 100;

    /**
     * @param string $actionId
     *
     * @return array
     */
    public function options($actionId)
    {
        return ArrayHelper::merge(parent::options($actionId), [
            'count',
        ]);
    }

    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     */
    public function beforeAction($action)
    {
        if (is_null($this->_faker)) {
            $this->_faker = Factory::create('ru_RU');
        }

        return parent::beforeAction($action);
    }

    /**
     * Main action
     */
    public function actionIndex()
    {
        $this->stdout(get_called_class() . '::' . __FUNCTION__);
    }
}