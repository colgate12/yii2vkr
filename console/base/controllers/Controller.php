<?php


namespace console\base\controllers;

use Yii;
use yii\console\Controller as YiiController;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;


/**
 * Class Controller
 * @package console\base\controllers
 */
abstract class Controller extends YiiController
{
    /**
     * @var int Количество элементов
     */
    public $limit = 10;

    /**
     * @var int Количество элементов в пачке
     */
    public $batch = 100;

    /**
     * @var bool Вносить ли изменения в базу
     */
    public $test = false;

    /**
     * @var \yii\db\Transaction Сохранность данных
     */
    protected $_transaction;

    /**
     * @var integer Время начала работы
     */
    protected $_startTime;

    /**
     * @var int Всего записей
     */
    protected $_totalRowsCount = 0;

    /**
     * @var int Успешных записей
     */
    protected $_successRowsCount = 0;

    /**
     * @var int Количество ошибок
     */
    protected $_errorRowsCount = 0;

    /**
     * @param string $actionId
     * @return array
     */
    public function options($actionId)
    {
        return ArrayHelper::merge(parent::options($actionId), [
            'limit', 'batch', 'test'
        ]);
    }

    /**
     * Создаем транзакцию
     * @param \yii\base\Action $action Запрошенное действие
     * @return bool Результат
     */
    public function beforeAction($action)
    {
        if (is_null($this->_startTime)) {
            $this->_startTime = microtime(true);
        }

        if ($this->test && is_null($this->_transaction)) {
            $this->_transaction = Yii::$app->db->beginTransaction();
            $this->stdPrint("Тестовый режим начат!", true, true, Console::FG_YELLOW, true, $this->_startTime);
        }

        return parent::beforeAction($action);
    }

    /**
     * Обрабатываем транзакцию
     * @param \yii\base\Action $action Запрошенное действие
     * @param mixed $result Результат
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function afterAction($action, $result)
    {
        if (!is_null($this->_transaction)) {
            if (!$this->test) {
                $this->_transaction->commit();
                $this->stdPrint("Изменения совершены!", true, true, Console::FG_GREEN, true);
            } else {
                $this->_transaction->rollBack();
                $this->stdPrint("Изменения отклонены!", true, true, Console::FG_RED, true);
            }
        }

        $this->stdPrint(' --- ', false, false, Console::FG_BLUE, true);
        if (!is_null($this->_startTime)) {
            $workTime = microtime(true) - $this->_startTime;
            $this->stdPrint('Рабочее время: ', false, false);
            $this->stdPrint("{$workTime}с.", true, false, Console::FG_PURPLE);
        }
        $this->stdPrint('Всего записей: ', false, false);
        $this->stdPrint($this->_totalRowsCount, true, false, Console::FG_PURPLE);
        $this->stdPrint('Успешных записей: ', false, false);
        $this->stdPrint($this->_successRowsCount, true, false, Console::FG_PURPLE);
        $this->stdPrint('Ошибочных записей: ', false, false);
        $this->stdPrint($this->_errorRowsCount, true, false, Console::FG_PURPLE);
        $this->stdPrint(' --- ', false, false, Console::FG_BLUE, true);

        return parent::afterAction($action, $result);
    }

    /**
     * Вывод сообщений в консоль с настройками
     * @param string $message Сообщение
     * @param bool|true $newLine Переход на новую строку
     * @param bool|true $withTime Клеить время
     * @param int $color Цвет текста
     * @param bool|false $isBlock обрамить пробелами
     * @param null $time Время, которое клеить
     */
    protected function stdPrint($message, $newLine = true, $withTime = true, $color = Console::FG_GREY, $isBlock = false, $time = null)
    {
        if($isBlock) {
            $this->stdout(PHP_EOL);
        }

        if ($withTime) {
            if(is_null($time)) {
                $time = time();
            }
            $this->stdout(date('H:i:s', $time) . ' ', Console::FG_PURPLE);
        }

        $this->stdout($message, $color);

        if ($newLine) {
            $this->stdout(PHP_EOL);
        }

        if($isBlock) {
            $this->stdout(PHP_EOL);
        }
    }

    /**
     * Сохранение записи
     * @param \common\db\ActiveRecord $model Модель, которую сохранять
     */
    protected function _save(&$model)
    {
        /** @var \common\db\ActiveRecord $model */

        if ($model->save()) {
            $this->_successRowsCount++;
            $this->stdPrint('Успешно!', true, false, Console::FG_GREEN);
        } else {
            $this->_errorRowsCount++;
            $this->stdPrint('Ошибка!', true, false, Console::FG_RED);
        }
    }

    /**
     * Удаление записи
     * @param \common\db\ActiveRecord $model Модель, которую сохранять
     */
    protected function _delete(&$model)
    {
        /** @var \common\db\ActiveRecord $model */

        if ($model->delete()) {
            $this->_successRowsCount++;
            $this->stdPrint('Успешно!', true, false, Console::FG_GREEN);
        } else {
            $this->_errorRowsCount++;
            $this->stdPrint('Ошибка!', true, false, Console::FG_RED);
        }
    }

}