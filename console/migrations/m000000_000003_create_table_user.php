<?php

use yii\db\Migration;

class m000000_000003_create_table_user extends Migration
{
    protected $_tableName = '{{%user}}';

    public function safeUp()
    {
        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey()->unsigned(),
            'network_key' => $this->smallInteger(1)->notNull()->defaultValue(0)->comment('Соц. сеть'),
            'network_id' => $this->string()->comment('ID профиля'),
            'email' => $this->string()->comment('Email'),
            'phone' => $this->string(100)->comment('Телефон'),
            'image_link' => $this->string()->comment('Аватар'),
            'first_name' => $this->string()->comment('Имя'),
            'last_name' => $this->string()->comment('Фамилия'),
            'middle_name' => $this->string()->comment('Отчество'),
            'ip' => $this->string(50)->comment('IP адрес'),
            'password_hash' => $this->string()->notNull()->comment('Хеш пароля'),
            'password_reset_token' => $this->string()->comment('Токен восстановления пароля'),
            'auth_key' => $this->string(32)->notNull()->comment('Ключ авторизации'),
            'sex_key' => $this->smallInteger(1)->notNull()->defaultValue(0)->comment('Пол'),
            'status_key' => $this->smallInteger(1)->notNull()->defaultValue(1)->comment('Статус'),
            'created_at' => $this->dateTime()->notNull()->comment('Дата создания'),
            'updated_at' => $this->dateTime()->notNull()->comment('Дата изменения'),
        ]);
    }
}
