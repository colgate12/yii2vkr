<?php

use yii\db\Migration;

class m000000_000001_create_table_admin extends Migration
{
    protected $_tableName = '{{%admin}}';

    public function safeUp()
    {
        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey()->unsigned(),
            'username' => $this->string(100)->notNull()->unique()->comment('Логин'),
            'email' => $this->string(100)->notNull()->unique()->comment('Email'),
            'first_name' => $this->string(50)->comment('Имя'),
            'last_name' => $this->string(50)->comment('Фамилия'),
            'password_hash' => $this->string()->notNull()->comment('Хеш пароля'),
            'password_reset_token' => $this->string()->comment('Токен восстановления пароля'),
            'auth_key' => $this->string(32)->notNull()->comment('Ключ авторизации'),
            'status_key' => $this->smallInteger(1)->notNull()->defaultValue(0)->comment('Статус'),
            'created_at' => $this->dateTime()->notNull()->comment('Дата создания'),
            'updated_at' => $this->dateTime()->notNull()->comment('Дата изменения'),
        ]);
    }
}
