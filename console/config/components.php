<?php

return [
    'admin' => [
        'class' => '\yii\web\User',
        'identityClass' => 'backend\models\Admin',
        'enableAutoLogin' => true,
    ],
    'authManager' => [
        'class' => 'yii\rbac\DbManager',
        'itemTable' => '{{%admin_auth_item}}',
        'itemChildTable' => '{{%admin_auth_item_child}}',
        'assignmentTable' => '{{%admin_auth_assignment}}',
        'ruleTable' => '{{%admin_auth_rule}}',
    ],
    'user' => [
        'class' => '\yii\web\User',
        'identityClass' => 'common\models\User',
        'enableAutoLogin' => true,
    ],
    'cache' => [
        'class' => 'yii\caching\FileCache',
        'cachePath' => '@frontend/runtime/cache'
    ],
];