<?php

namespace backend\controllers;

use backend\base\controllers\Controller;
use backend\forms\DashboardForm;
use Yii;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @var bool
     */
    public $modelClass = false;

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['/user']);
    }
}
