<?php

use common\helpers\Param;
use common\widgets\Menu;

?>

<aside class="main-sidebar">
    <section class="sidebar">
        <?=Menu::widget([
            'options'         => ['class' => 'sidebar-menu'],
            'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
            'activateParents' => true,
            'items'           => Param::get('backend.menu'),
        ])?>
    </section>
</aside>
