<?php

use backend\assets\MainAsset;
use dmstr\helpers\AdminLteHelper;
use dmstr\web\AdminLteAsset;
use yii\helpers\Html;


/**
 * @var \yii\web\View $this
 * @var string $content
 */

AdminLteAsset::register($this);
MainAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>"/>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <?= Html::csrfMetaTags() ?>
            <title><?= Html::encode($this->title) ?></title>
            <?php $this->head() ?>
        </head>
        <body class="<?= AdminLteHelper::skinClass() ?> sidebar-mini">
            <?php $this->beginBody() ?>
                <div id=main_wrapper>
                    <?= $this->render(
                        'header',
                        ['directoryAsset' => $directoryAsset]
                    ) ?>

                    <?= $this->render(
                        'left',
                        ['directoryAsset' => $directoryAsset]
                    )
                    ?>

                    <?= $this->render(
                        'content',
                        ['content' => $content, 'directoryAsset' => $directoryAsset]
                    ) ?>

                    <?= $this->render(
                        'footer',
                        ['directoryAsset' => $directoryAsset]
                    ) ?>
                </div>
            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>
