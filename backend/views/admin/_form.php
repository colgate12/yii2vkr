<?php

use backend\models\Admin;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/**
 * @var yii\web\View $this
 * @var backend\models\Admin $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<?php $form = ActiveForm::begin([
	'options' => ['enctype' => 'multipart/form-data'],
]); ?>
	<div class="box-body">
		<legend>Основные свойства</legend>
		<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'password_confirm')->textInput(['maxlength' => true]) ?>

		<?php
			echo $form->field($model, 'role')->widget(Select2::classname(), [
				'data' => ArrayHelper::map(Admin::roleList(), 'name', 'name'),
				'pluginOptions' => [
					'allowClear' => false
				],
			]);
		?>

		<?php if(!$model->isNewRecord) : ?>
			<legend>Дополнительная информация</legend>
			<?= $form->field($model, 'created_at')->textInput(['disabled' => true]) ?>
			<?= $form->field($model, 'updated_at')->textInput(['disabled' => true]) ?>
		<?php endif; ?>
	</div>

	<div class="box-footer">
		<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>
<?php ActiveForm::end(); ?>
