<?php

use common\widgets\DynaGrid;


/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\Admin $model
 */

$this->title = 'Админы';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
	[
		'attribute' => 'username',
	],
	[
		'attribute' => 'email',
	],
	[
		'attribute' => 'authAssignment.role.name',
		'header' => 'Роль'
	],
];
?>

<?= DynaGrid::widget([
	'gridId' => 'dynagrid-admin',
	'columns' => $columns,
	'gridOptions' => [
		'dataProvider' => $dataProvider,
		'filterModel' => $model,
	]
]) ?>
