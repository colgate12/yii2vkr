<?php

/**
 * @var yii\web\View $this
 * @var backend\models\Admin $model
 */

$this->title = 'Изменение: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Админы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
