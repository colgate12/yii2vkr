<?php

use common\widgets\DynaGrid;
use kartik\widgets\Select2;
use yii\helpers\Html;


/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\User $model
 */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
	[
		'attribute' => 'network_key',
		'format' => 'raw',
		'filter' => Select2::widget([
			'model' => $model,
			'attribute' => 'network_key',
			'options' => [
				'prompt' => '',
			],
			'data' => $model->getNetworks(),
			'pluginOptions' => [
				'allowClear' => true
			],
		]),
		'value' => function($model) {
			/** @var \common\models\User $model */
			return $model->getNetworkUrl() ? Html::a($model->getNetwork(), $model->getNetworkUrl(), ['target' => '_blank']) : $model->getNetwork();
		}
	],
	[
		'attribute' => 'network_id',
		'format' => 'raw',
		'value' => function($model) {
			/** @var \common\models\User $model */
			return $model->getNetworkUrl() ? Html::a($model->network_id, $model->getNetworkUrl(), ['target' => '_blank']) : $model->network_id;
		}
	],
	[
		'attribute' => 'first_name',
	],
	[
		'attribute' => 'last_name',
	],
	[
		'attribute' => 'email',
	],
	[
		'attribute' => 'status_key',
		'filter' => Select2::widget([
			'model' => $model,
			'attribute' => 'status_key',
			'options' => [
				'prompt' => '',
			],
			'data' => $model::getStatuses(),
			'pluginOptions' => [
				'allowClear' => true
			],
		]),
		'value' => function($model) {
			return $model->status;
		}
	],
];
?>

<?= DynaGrid::widget([
    'gridId' => 'dynagrid-user',
    'columns' => $columns,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
		'detailColumnEnabled' => true,
		'toolbarApproveButtonEnabled' => true,
		'toolbarDisapproveButtonEnabled' => true,
		'actionApproveEnabled' => true,
		'actionDisapproveEnabled' => true,
    ],
]) ?>
