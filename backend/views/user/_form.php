<?php

use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var backend\models\User $model
 */

?>

<?php $form = ActiveForm::begin([
	'options' => ['enctype' => 'multipart/form-data'],
]); ?>
	<div class="box-body">
		<legend>Основные свойства</legend>

		<?= $form->field($model, 'network_key')->widget(Select2::classname(), [
			'data' => $model::getNetworks(),
			'options' => [
				'prompt' => '',
			],
			'pluginOptions' => [
				'allowClear' => true
			],
		]) ?>

		<?= $form->field($model, 'network_id')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

		<div class="form-group">
			<?= $form->field($model, 'file_image_link')->widget(FileInput::className(), [
				'options' => ['accept' => 'image/*'],
				'pluginOptions' => [
					'initialPreview' => !empty($model->image_link) ? Html::img($model->getImageUrl(), ['class'=>'file-preview-image']) : '',
					'overwriteInitial' => true,
					'showRemove' => false,
					'showUpload' => false
				]
			]) ?>
		</div>

		<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'password_confirm')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'status_key')->widget(Select2::classname(), [
			'data' => $model::getStatuses(),
			'pluginOptions' => [
				'allowClear' => false
			],
		]) ?>

		<?php if(!$model->isNewRecord) : ?>
			<legend>Дополнительная информация</legend>
			<?= $form->field($model, 'auth_key')->textInput(['disabled' => true]) ?>
			<?= $form->field($model, 'created_at')->textInput(['disabled' => true]) ?>
			<?= $form->field($model, 'updated_at')->textInput(['disabled' => true]) ?>
		<?php endif; ?>
	</div>
	<div class="box-footer">
		<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>
<?php ActiveForm::end(); ?>
