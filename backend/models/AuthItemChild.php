<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%admin_auth_item_child}}".
 *
 * @property string $parent
 * @property string $child
 */
class AuthItemChild extends ActiveRecord
{
    /**
     * @return string Название таблицы
     */
    public static function tableName()
    {
        return '{{%admin_auth_item_child}}';
    }

    /**
     * @return array Правила валидации
     */
    public function rules()
    {
        return [
            [['parent', 'child'], 'required'],
            [['parent', 'child'], 'string', 'max' => 64],
        ];
    }

    /**
     * @return array Надписи атрибутов
     */
    public function attributeLabels()
    {
        return [
            'parent' => 'Parent',
            'child' => 'Child',
        ];
    }
}
