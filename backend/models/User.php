<?php

namespace backend\models;

use common\models\User as CommonUser;
use yii\helpers\Url;


/**
 * Class User
 * @package backend\models
 */
class User extends CommonUser
{
    /**
     * @param bool $isAbsolute
     *
     * @return string
     */
    public function getUpdateUrl($isAbsolute = false)
    {
        return Url::to(['/user/update', 'id' => $this->id], $isAbsolute);
    }

}