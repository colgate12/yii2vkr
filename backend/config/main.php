<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php'),
    require(__DIR__ . '/params-menu.php')
);

$config = [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'site/index',
    'name' => 'Develup',
    'language' => 'ru-RU',
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['session'],
    'modules' => require(__DIR__.'/modules.php'),
    'components' => require(__DIR__.'/components.php'),
    'params' => $params,
];

return $config;
