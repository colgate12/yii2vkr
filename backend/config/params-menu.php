<?php

return [
    // Backend Menu Items
    'backend.menu' => [
        [
            'label' => 'Пользователи',
            'icon'  => '<i class="fa fa-user"></i>',
            'url'   => ['/user/index'],
        ],
        [
            'label' => 'Админы',
            'icon'  => '<i class="fa fa-user-secret"></i>',
            'url'   => ['/admin/index'],
        ],
//        [
//            'label' => 'Правила сайта',
//            'icon'  => '<i class="fa fa-list-alt"></i>',
//            'url'   => ['/rules/index'],
//        ],
        [
            'label' => 'Настройки',
            'icon'  => '<i class="fa fa-cogs"></i>',
            'url'   => ['/setting/index', 'section' => 'general'],
        ],
//        [
//            'label' => 'Импорт',
//            'icon'  => '<i class="fa fa-upload"></i>',
//            'url'   => ['/impex/import/index'],
//        ],
//        [
//            'label' => 'Экспорт',
//            'icon'  => '<i class="fa fa-download"></i>',
//            'url'   => ['/impex/export/index'],
//        ],
    ],

    // Settings Menu Items
    'settings.menu' => [
        ['label' => 'Основные', 'url' => ['/setting/index', 'section' => 'general']],
    ]
];