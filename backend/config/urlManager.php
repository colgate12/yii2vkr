<?php
return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        'logout' => 'auth/logout',
        'login' => 'auth/login',

        '<controller:[\w-]+>' => '<controller>/index',
        '<controller:[\w-]+>/<id:\d+>' => '<controller>/update',
        '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',
        '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',
    ],
];