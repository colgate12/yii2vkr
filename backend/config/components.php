<?php

return [
    'request' => [
        'enableCsrfValidation'=>false,
    ],
    'assetManager' => [
        'bundles' => [
            'dmstr\web\AdminLteAsset' => [
                'skin' => 'skin-blue',
            ],
        ],
    ],
    'user' => [
        'loginUrl' => ['auth/login'],
        'identityClass' => 'backend\models\Admin',
        'enableAutoLogin' => true,
        'identityCookie' => [
            'name' => '_backendUser', // unique for backend
            'path'=>'/backend/web'  // correct path for the backend app.
        ],
    ],
    'session' => [
        'name' => '_backendSessionId', // unique for backend
    ],
    'authManager' => [
        'class' => 'yii\rbac\DbManager',
        'itemTable' => '{{%admin_auth_item}}',
        'itemChildTable' => '{{%admin_auth_item_child}}',
        'assignmentTable' => '{{%admin_auth_assignment}}',
        'ruleTable' => '{{%admin_auth_rule}}',
    ],
    'cache' => [
        'class' => 'yii\caching\FileCache',
        'cachePath' => '@frontend/runtime/cache'
    ],
    'errorHandler' => [
        'errorAction' => 'system/error',
    ],
    'urlManager' => require(__DIR__.'/urlManager.php'),
];