<?php

return [
    'dynagrid'=> [
        'class'=>'\common\widgets\DynaGridModule',
    ],
    'gridview'=> [
        'class'=>'\kartik\grid\Module',
    ],
    'impex'=> [
        'class'=>'\backend\modules\impex\Module',
    ],
];