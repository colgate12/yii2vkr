<?php

namespace backend\forms;

use common\helpers\Param;
use platx\filehelper\FileHelper;
use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\UploadedFile;


/**
 * Class RulesForm
 * @package backend\forms
 */
class RulesForm extends Model
{
    /**
     * @var
     */
    public $file;

    /**
     * @var
     */
    protected $_fileUrl;

    /**
     * @var
     */
    protected $_filePath;

    /**
     * @throws \yii\web\NotFoundHttpException
     */
    public function init()
    {
        $this->_fileUrl = Param::get('rules.url');
        $this->_filePath = Yii::getAlias("@frontend/web{$this->_fileUrl}");

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'file',
                'file',
                'extensions' => 'pdf',
                'maxSize' => 5 * 1204 *1024,
                'enableClientValidation' => false,
                'checkExtensionByMimeType' => false,
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
        ];
    }

    /**
     * @param bool|false $isAbsolute
     * @return bool|string
     * @throws \yii\web\NotFoundHttpException
     */
    public function getFileUrl($isAbsolute = false)
    {
        if(!file_exists($this->_filePath)) {
            return false;
        }

        return Url::to($this->_fileUrl, $isAbsolute);
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function upload()
    {
        if($this->validate()) {
            $file = UploadedFile::getInstance($this, 'file');

            if ($file instanceof UploadedFile) {
                if($file->hasError) {
                    $message = 'Ошибка загрузки файла';
                    switch ($file->error) {
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            $message .= ' - файл сильно большой.';
                            break;
                        case UPLOAD_ERR_PARTIAL:
                            $message .= ' - загрузка прервана.';
                            break;
                        case UPLOAD_ERR_NO_FILE:
                            $message .= ' - нет файла.';
                            break;
                        default:
                            $message .= ' - неизвестная ошибка.';
                            break;
                    }
                    $this->addError('file', $message);
                    return false;
                }
                if(FileHelper::createDirectory(dirname($this->_filePath))) {
                    if($file->saveAs($this->_filePath, true)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}