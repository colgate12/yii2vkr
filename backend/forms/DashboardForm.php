<?php

namespace backend\forms;

use common\helpers\Html;
use common\helpers\SystemHelper;
use Yii;
use yii\base\Model;


/**
 * Статистика
 * @package backend\forms
 */
class DashboardForm extends Model
{
    /**
     * @return string
     */
    public function getSystemStat()
    {
        $memoryUsage = SystemHelper::getMemoryUsage();
        $cpuUsage = SystemHelper::getCpuUsage();
        $hddUsage = SystemHelper::getHddUsage();

        Yii::$app->view->registerJsFile("//www.google.com/jsapi");
        Yii::$app->view->registerJsFile("//www.gstatic.com/charts/loader.js");
        Yii::$app->view->registerJs("
            google.charts.load('current', {'packages':['gauge']});
            google.charts.setOnLoadCallback(drawChart);
        
            function drawChart() {
        
              var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['Memory', {$memoryUsage}],
                ['CPU', {$cpuUsage}],
                ['HDD', {$hddUsage}]
              ]);
        
              var options = {
                width: 400, height: 120,
                redFrom: 90, redTo: 100,
                yellowFrom:75, yellowTo: 90,
                minorTicks: 5
              };
        
              var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
        
              chart.draw(data, options);
            }
        ");

        return Html::tag('div', '', [
            'id' => 'chart_div',
            'style' => 'width: 400px; height: 120px;'
        ]);
    }
}