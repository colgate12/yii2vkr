<?php

namespace backend\base\controllers;

use common\db\ActiveRecord;
use Exception;
use himiklab\sortablegrid\SortableGridAction;
use Yii;
use yii\base\ErrorException;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use platx\httperror\HttpError;
use common\helpers\AlertHelper;
use yii\web\Controller as YiiController;
use yii\web\MethodNotAllowedHttpException;


/**
 * Базовый контроллер для админки
 * @package backend\base\controllers
 */
abstract class Controller extends YiiController
{
    /**
     * Модель с которой работает контроллер
     * @var string
     */
    public $modelClass = null;

    /**
     * Ссылки редиректа после действий
     */
    public $urlAfterCreate = 'index';
    public $urlAfterUpdate = 'index';
    public $urlAfterDelete = 'index';
    public $urlAfterApprove = 'index';
    public $urlAfterDisapprove = 'index';

    public $disabledActions = [];

    /**
     * Подключение общих поведений
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'moder'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Initialization backend controller
     * @throws \yii\web\HttpException
     */
    public function init()
    {
        if ($this->modelClass === null) {
            throw new InvalidConfigException(get_class($this) . '::$modelClass must be set.');
        }

        parent::init();
    }

    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     * @throws MethodNotAllowedHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $controllerId = $action->controller->id;
        $actionId = $action->id;

        if (in_array($actionId, $this->disabledActions)) {
            throw new MethodNotAllowedHttpException("Метод отключен!");
        }

        $currentUrl = Yii::$app->request->url;

        Url::remember($currentUrl, "{$controllerId}_{$actionId}");

        return parent::beforeAction($action);
    }

    /**
     * @return array
     */
    public function actions()
    {
        $items = parent::actions();

        /*if($this->modelClass) {
            $items['sort'] = [
                'class' => SortableGridAction::className(),
                'modelName' => $this->modelClass,
            ];
        }*/

        return $items;
    }

    /**
     * Вывод списка моделей
     * @return string
     */
    public function actionIndex()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;
        $get = Yii::$app->request->get();
        $dataProvider = $model->search($get);

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Создание модели
     * @return string
     */
    public function actionCreate()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;
        $post = Yii::$app->request->post();
        $get = Yii::$app->request->get();

        if ($get) {
            $model->load($get, '');
        }

        if ($model->load($post)) {
            if ($model->save()) {
                AlertHelper::success('Добавлено успешно!');
                $backUrl = Url::previous("{$this->id}_{$this->urlAfterCreate}");

                if ($backUrl) {
                    $this->redirect($backUrl);
                } else {
                    $this->redirect([$this->urlAfterCreate]);
                }
            } else {
                AlertHelper::error("Ошибка добавления! \n" . print_r($model->errors, true));
            }
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * Обновление модели
     * @param $id
     * @return string
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        $get = Yii::$app->request->get();

        if ($get) {
            $model->load($get, '');
        }

        if ($model->load($post)) {
            if ($model->save()) {
                AlertHelper::success('Успешно сохранено!');
                $backUrl = Url::previous("{$this->id}_{$this->urlAfterUpdate}");

                if ($backUrl) {
                    $this->redirect($backUrl);
                } else {
                    $this->redirect([$this->urlAfterUpdate]);
                }
            } else {
                AlertHelper::error("Ошибка сохранения! \n" . print_r($model->errors, true));
            }
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Удаление модели
     * @param $id
     * @throws ErrorException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if ($model->delete()) {
                AlertHelper::success('Успешно удалено!');
            } else {
                AlertHelper::error('Ошибка удаления!');
            }
        } catch (Exception $ex) {
            AlertHelper::error('Невозможно удалить, используется в связях.');
        }

        if (!Yii::$app->request->isAjax) {
            $backUrl = Url::previous("{$this->id}_{$this->urlAfterDelete}");

            if ($backUrl) {
                $this->redirect($backUrl);
            } else {
                $this->redirect([$this->urlAfterDelete]);
            }
        }
    }

    /**
     * Удаление модели
     * @param $id
     * @throws ErrorException
     */
    public function actionApprove($id)
    {
        $model = $this->findModel($id);

        if ($model->approve()) {
            AlertHelper::success('Успешно опубликовано!');
        } else {
            AlertHelper::error("Ошибка сохранения! \n" . print_r($model->errors, true));
        }

        if (!Yii::$app->request->isAjax) {
            $backUrl = Url::previous("{$this->id}_{$this->urlAfterApprove}");

            if ($backUrl) {
                $this->redirect($backUrl);
            } else {
                $this->redirect([$this->urlAfterApprove]);
            }
        }
    }

    /**
     * Удаление модели
     * @param $id
     * @throws ErrorException
     */
    public function actionDisapprove($id)
    {
        $model = $this->findModel($id);

        if ($model->disapprove()) {
            AlertHelper::success('Успешно убрано!');
        } else {
            AlertHelper::error("Ошибка сохранения! \n" . print_r($model->errors, true));
        }

        if (!Yii::$app->request->isAjax) {
            $backUrl = Url::previous("{$this->id}_{$this->urlAfterDisapprove}");

            if ($backUrl) {
                $this->redirect($backUrl);
            } else {
                $this->redirect([$this->urlAfterDisapprove]);
            }
        }
    }

    /**
     * @return int
     */
    public function actionDeleteSelected()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;

        $keys = Yii::$app->request->post('selectedIds');

        $keys = explode(',', $keys);

        return $model->deleteAll(['id' => $keys]);
    }

    /**
     * @return int
     */
    public function actionApproveSelected()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;

        $keys = Yii::$app->request->post('selectedIds');

        $keys = explode(',', $keys);

        return $model->approveAll(['id' => $keys]);
    }

    /**
     * @return int
     */
    public function actionDisapproveSelected()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;

        $keys = Yii::$app->request->post('selectedIds');

        $keys = explode(',', $keys);

        return $model->disapproveAll(['id' => $keys]);
    }

    /**
     * Детальный просмотр записи в списке
     * @return string
     */
    public function actionDetailView()
    {
        $id = Yii::$app->request->post('expandRowKey');

        $model = $this->findModel($id);

        if(empty($model)) {
            return '<div class="alert alert-danger">Нет данных</div>';
        }

        return $this->renderPartial('detail-view', ['model' => $model]);
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws ErrorException
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;

        if(is_array($id) && count($id) > 1) {
            throw new ErrorException('Composite foreign keys are not allowed.');
        }

        $model = $model::findByPk($id);

        if(!$model) {
            HttpError::the404();
        }

        return $model;
    }
}