<?php

namespace frontend\forms;

use common\models\User;
use yii\base\Model;
use Yii;


/**
 * Register form
 */
class RegisterForm extends Model
{
    public $first_name;
    public $last_name;
    public $age;
    public $region_id;
    public $city_id;
    public $email;
    public $password;
    public $password_confirm;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['first_name', 'last_name', 'email'], 'filter', 'filter' => 'trim'],
	        ['age', 'integer'],

            ['first_name', 'required', 'message' => 'Введите имя'],
            ['last_name', 'required', 'message' => 'Введите фамилию'],
            ['age', 'required', 'message' => 'Введите Ваш возраст'],
            ['region_id', 'required', 'message' => 'Выберите регион'],
            ['city_id', 'required', 'message' => 'Выберите город'],
            ['email', 'required', 'message' => 'Введите email'],

	        [['region_id', 'city_id'], 'integer'],

            ['email', 'unique', 'targetClass' => 'frontend\modules\user\models\PasswordIdentity', 'message' => 'Этот email уже зарегистрирован'],
            ['email', 'email'],

            [['password', 'password_confirm'], 'required'],
	        ['password_confirm', 'compare', 'compareAttribute' => 'password'],
        ];
    }

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		return [
			'first_name' => 'Имя',
			'last_name' => 'Фамилия',
			'age' => 'Возраст',
			'region_id' => 'Регион',
			'city_id' => 'Город',
			'email' => 'E-mail',
			'password' => 'Пароль',
			'password_confirm' => 'Подтверждение пароля',
		];
	}


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signUp()
    {
        if ($this->validate()) {
			$userModel = new User();

			$userModel->setAttributes([
				'first_name' => $this->first_name,
				'last_name' => $this->last_name,
				'email' => $this->email,
				'password' => $this->password,
				'password_confirm' => $this->password_confirm,
			]);
			$userModel->generateAuthKey();
			$userModel->setPassword($this->password);

			if($userModel->save()) {

					return $userModel;
			}
        }

        return null;
    }
}
