<?php

namespace frontend\forms;

use common\helpers\Param;
use common\models\User;
use Yii;
use yii\base\Model;


/**
 * Форма для запроса восстановления пароля
 * @package frontend\forms
 */
class PasswordResetRequestForm extends Model
{
    /**
     * @var string E-mail для восстановления пароля
     */
    public $email;

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'message' => 'Пользователь с таким адресом электронной почты не зарегистрирован.'
            ],
        ];
    }

    /**
     * Отправляет токен для сброса пароля на указанный e-mail
     * @return bool
     */
    public function sendEmail()
    {
        /* @var $model User */
        $model = User::findOne([
            'email' => $this->email,
        ]);

        if ($model) {
            if (!User::isPasswordResetTokenValid($model->password_reset_token)) {
                $model->generatePasswordResetToken();
            }

            if ($model->save()) {
                return Yii::$app->mailer->compose(['html' => 'password-reset-token-html', 'text' => 'password-reset-token-text'], ['model' => $model])
                    ->setFrom(Param::get('adminEmail'))
                    ->setTo($this->email)
                    ->setSubject('Восстановление пароля на сайте ' . Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}
