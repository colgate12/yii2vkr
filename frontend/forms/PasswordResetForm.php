<?php

namespace frontend\forms;

use common\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;


/**
 * Форма для восстановления пароля
 * @package frontend\forms
 */
class PasswordResetForm extends Model
{
    /**
     * @var string Новый пароль
     */
    public $password;

    /**
     * @var string Подтверждение нового пароля
     */
    public $password_confirm;

    /**
     * @var string Токен для восстановления пароля
     */
    public $token;

    /**
     * @var User
     */
    private $_user;

    /**
     * Инициализация
     */
    public function init()
    {
        $this->_user = User::findByPasswordResetToken($this->token);

        if (!$this->_user) {
            throw new InvalidParamException('Wrong password reset token.');
        }

        parent::init();
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return [
            [['password', 'password_confirm'], 'required'],
            [['password', 'password_confirm'], 'string', 'min' => 6],
            ['password_confirm', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * Устанавливает новый пароль для авторизации
     * @return bool
     */
    public function changePassword()
    {
        $model = $this->_user;
        $model->setPassword($this->password);
        $model->removePasswordResetToken();

        if($model->save(false) && Yii::$app->user->login($model->user, 3600*60*60)) {
            return true;
        }

        return false;
    }
}
