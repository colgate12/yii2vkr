<?php

namespace frontend\forms;

use common\models\User;
use Yii;
use yii\base\Model;


/**
 * Login form
 */
class LoginForm extends Model {
    public $email;
    public $password;
    public $rememberMe = true;

    private $_identity = false;


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['email', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'         => 'ID',
            'email'      => 'Email',
            'password'   => 'Пароль',
            'rememberMe' => 'Запомнить меня',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute) {
        if (!$this->hasErrors()) {
            $identity = $this->getIdentity();
            if (!$identity || !$identity->validatePassword($this->password)) {
                $this->addError($attribute, 'Пользователя с такими логином и паролем не существует');
            }
        }
    }

    /**
     * Logs in a user using the provided login and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login() {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getIdentity()->user, $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getIdentity()
    {
        if ($this->_identity === false) {
            $this->_identity = User::findByEmail($this->email);
        }

        return $this->_identity;
    }
}
