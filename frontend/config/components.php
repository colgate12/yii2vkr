<?php

return [
    'request' => [
        'parsers' => [
            'application/json' => 'yii\web\JsonParser',
        ]
    ],
    'user' => [
        'identityClass' => 'common\models\User',
        'enableAutoLogin' => true,
        'loginUrl' => null,
        'identityCookie' => [
            'name' => '_frontendUser', // unique for frontend
            'path'=>'/frontend/web'  // correct path for the frontend app.
        ],
    ],
    'session' => [
        'name' => '_frontendSessionId', // unique for frontend
    ],
    'authManager' => [
        'class' => 'yii\rbac\DbManager',
        'itemTable' => '{{%user_auth_item}}',
        'itemChildTable' => '{{%user_auth_item_child}}',
        'assignmentTable' => '{{%user_auth_assignment}}',
        'ruleTable' => '{{%user_auth_rule}}',
    ],
    'view' => [
        'class' => 'yii\web\View',
        'renderers' => [
            'twig' => [
                'class' => 'yii\twig\ViewRenderer',
                'cachePath' => '@runtime/Twig/cache',
                'options' => [
                    'auto_reload' => true,
                ],
                'globals' => [
                    'html' => '\common\helpers\Html',
                    'url' => '\yii\helpers\Url',
                    'Yii' => '\Yii',
                ],
                'uses' => ['yii\bootstrap'],
            ],
        ],
    ],
    'urlManager' => require(__DIR__.'/urlManager.php'),
];