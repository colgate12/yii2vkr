<?php
return [
	'class'=>'yii\web\UrlManager',
	'enablePrettyUrl'=>true,
	'showScriptName'=>false,
	'rules'=>[
		'uploads/<width:\d+>x<height:\d+>/<model:[\w_]+>/<field:[\w-]+>/<dynamicPath:[\d\/]+>/<file:[\w-]+>.<ext:\w+>' => 'system/uploads',
		'uploads/original/<model:[\w_]+>/<field:[\w-]+>/<dynamicPath:[\d\/]+>/<file:[\w-]+>.<ext:\w+>' => 'system/uploads',

		'<controller:system>/<action:[\w\d-_/]+>' => '<controller>/<action>',
		'<controller:vote>/<action:[\w\d-_/]+>' => '<controller>/<action>',

		'<controller:auth>/<action:social>/<authclient:[\w\d-_/]+>' => '<controller>/<action>',
		'<controller:auth>/<action:logout>' => '<controller>/<action>',

		'<url:[\w\d-_/]+>/' => 'site/index',
		'<url:[\w\d-_/]+>' => 'site/index',
	]
];