<?php

namespace frontend\base\controllers;

use yii\web\Controller as YiiController;


/**
 * Frontend base controller
 * @package frontend\base\controllers
 */
abstract class Controller extends YiiController
{

}