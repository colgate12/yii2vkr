<?php

namespace frontend\controllers;

use frontend\base\controllers\Controller;
use Yii;


/**
 * Main controller
 */
class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (YII_ENV == 'dev') {
            Yii::$app->response->getHeaders()->set('Access-Control-Allow-Origin', '*');
        }
        return $this->renderFile('@frontend/web/main.php');
    }
}
