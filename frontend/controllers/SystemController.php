<?php

namespace frontend\controllers;

use common\helpers\Param;
use common\models\User;
use frontend\base\controllers\Controller;
use platx\httperror\HttpError;
use Yii;


/**
 * Class SystemController
 * @package frontend\controllers
 */
class SystemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'easter-egg' => [
                'class' => 'platx\easteregg\EasterEggAction',
            ],
            'uploads' => [
                'class' => 'platx\uploads\UploadsAction',
                'sizeList' => Param::get('uploads.size_list'),
            ],
        ];
    }

    /**
     * @param $token
     * @param $id
     *
     * @return bool
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionLogin($token, $id = null)
    {
        if (is_null($id)) {
            $userModel = User::find()->andWhere(['auth_key' => $token])->one();
        } else {
            if ($token != Param::get('dev.token')) {
                HttpError::the403('Доступ запрещен!');
            }

            $userModel = User::findByPk($id);
        }

        if (!$userModel) {
            HttpError::the404('Пользователь не найден!');
        }

        Yii::$app->user->login($userModel, 3600*24*7);

        return $this->redirect('/');
    }
}