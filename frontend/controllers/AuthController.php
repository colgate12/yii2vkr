<?php

namespace frontend\controllers;

use common\models\User;
use frontend\base\controllers\Controller;
use platx\httperror\HttpError;
use Yii;
use yii\authclient\AuthAction;
use yii\authclient\BaseClient;
use yii\web\Response;


/**
 * Auth controller
 */
class AuthController extends Controller
{
    /**
     * @return array Действия
     */
    public function actions()
    {
        return [
            'social' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @param AuthAction $client
     *
     * @return string
     */
    public function onAuthSuccess($client) {
        /** @var BaseClient $attributes */
        $attributes = $client->getUserAttributes();

        $sourceId = isset($attributes['id']) ? $attributes['id']: '';
        $attributes['client'] = $client->getId();

        $params = [
            'network_id' => (string)$sourceId,
            'first_name' => isset($attributes['first_name']) ? $attributes['first_name'] : '',
            'last_name' => isset($attributes['last_name']) ? $attributes['last_name'] : '',
            'middle_name' => isset($attributes['middle_name']) ? $attributes['middle_name'] : '',
            'phone' => isset($attributes['phone']) ? $attributes['phone'] : '',
            'media_link' => isset($attributes['avatar_url']) ? $attributes['avatar_url'] : '',
            'email' => isset($attributes['email']) ? $attributes['email'] : null,
        ];

        switch($attributes['client']) {
            case 'vkontakte' :
                $params['network_key'] = User::NETWORK_VK;
                break;
            case 'facebook' :
                $params['network_key'] = User::NETWORK_FB;
                break;
            case 'odnoklassniki' :
                $params['network_key'] = User::NETWORK_OK;
                break;
            case 'twitter' :
                $params['network_key'] = User::NETWORK_TW;
                break;
            case 'instagram' :
                $params['network_key'] = User::NETWORK_IG;
                break;
        }

        /** @var User $user */
        $userModel = User::findByNetwork($params['network_key'], $params['network_id']);

        if (!$userModel) { // login
            $userModel = new User();
            $password = uniqid();
            $userModel->password = $password;
            $userModel->status_key = User::STATUS_ENABLED;
            $userModel->generateAuthKey();
            $userModel->generatePasswordResetToken();
        } else {
            if (!$userModel->getIsActive()) {
                HttpError::the403('Доступ запрещен');
            }
        }

        $userModel->attributes = $params;
        $userModel->image_link = $params['media_link'];
        $userModel->scenario = 'social';

        if($userModel->save()) {
            Yii::$app->user->login($userModel, 3600 * 24 * 30);
        } else {
            HttpError::the500();
        }

        return new Response([
            'format' => Response::FORMAT_HTML,
            'charset' => 'UTF-8',
            'content' => $this->renderPartial('social', ['data' => $attributes])
        ]);
    }
}
