<?php

namespace frontend\components\authclients;


/**
 * Class Vkontakte
 * @package frontend\components\authclients
 */
class Vkontakte extends \yii\authclient\clients\VKontakte
{
    /**
     * @inheritdoc
     */
    protected function initUserAttributes()
    {
        $attributes = parent::initUserAttributes();

        if(isset($attributes['city'])) {
            $response = $this->api('database.getCitiesById', 'GET', [
                'city_ids' => $attributes['city'],
            ]);
            $cityAttributes = array_shift($response['response']);

            if(isset($cityAttributes['name'])) {
                $attributes['city'] = $cityAttributes['name'];
            }
        }

        return $attributes;
    }
}