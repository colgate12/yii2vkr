<?php

use yii\helpers\Json;
use yii\helpers\Url;


/**
 * @var \yii\base\View $this
 * @var string $url
 * @var boolean $enforceRedirect
 */

$url = Url::toRoute('/');
$enforceRedirect = false;

?>

<!DOCTYPE html>
<html>
	<head>
		<script>
			function popupWindowRedirect(url, enforceRedirect)
			{
				if (window.opener && !window.opener.closed) {
					if (enforceRedirect === undefined || enforceRedirect) {
	//					window.opener.location = url;
					}
					window.opener.focus();
					window.close();
				} else {
					window.location = url;
				}
			}
			popupWindowRedirect(<?= Json::htmlEncode($url) ?>, <?= Json::htmlEncode($enforceRedirect) ?>);
		</script>
	</head>
	<body>
		<h3 id="link"><a href="<?= $url ?>">Нажмите сюда для перехода на сайт.</a></h3>
		<script type="text/javascript">
			document.getElementById('title').style.display = '';
		//	document.getElementById('link').style.display = 'none';
		</script>
	</body>
</html>