<?php

use yii\helpers\Html;


/**
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception  $exception
 */

$this->title = $name;

?>

<div>
    <h1><?= Html::encode($this->title) ?></h1>

    <div>
        <?= nl2br(Html::encode($message)) ?>
    </div>
</div>
