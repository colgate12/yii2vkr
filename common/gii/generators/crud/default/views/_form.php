<?php

/* @var $this yii\web\View */
/* @var $generator common\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

unset($safeAttributes['created_at'], $safeAttributes['updated_at'], $safeAttributes['position']);

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var <?= ltrim($generator->modelClass, '\\') ?> $model
* @var yii\widgets\ActiveForm $form
*/

?>

<?= "<?php " ?>$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
]); ?>
    <div class="box-body">
<?php foreach ($generator->getColumnNames() as $attribute) {
if (in_array($attribute, $safeAttributes)) {
    echo "        <?= " . $generator->generateActiveField($attribute) . " ?>\n";
}
} ?>
    </div>
    <div class="box-footer">
        <?= "<?= " ?>Html::submitButton($model->isNewRecord ? <?= $generator->generateString('Добавить') ?> : <?= $generator->generateString('Обновить') ?>, ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?= "<?php " ?>ActiveForm::end(); ?>
