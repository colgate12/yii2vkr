<?php

/* @var $this yii\web\View */
/* @var $generator common\gii\generators\crud\Generator */

$contentShortColumn = $generator->getTableSchema()->getColumn('content_short');
$contentFullColumn = $generator->getTableSchema()->getColumn('content_full');

echo "<?php\n";
?>

/**
* @var yii\web\View $this
* @var <?= ltrim($generator->modelClass, '\\') ?> $model
*/

?>

<div class="row">
<?php if ($generator->getTableSchema()->getColumn('image_link')) : ?>
    <div class="col-lg-6">
        <div><?= "<?= " ?>$model->image_link ? Html::img($model->getImageUrl()) : '' ?></div>
    </div>
<?php endif; ?>
<?php if ($contentShortColumn || $contentFullColumn) : ?>
    <div class="col-lg-6">
<?php if ($contentShortColumn) : ?>
        <div><?= "<?= " ?>$model->content_short ?></div>
<?php endif; ?>
<?php if ($contentFullColumn) : ?>
        <div><?= "<?= " ?>$model->content_full ?></div>
<?php endif; ?>
    </div>
<?php endif; ?>
</div>