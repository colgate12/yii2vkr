<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator common\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use common\widgets\DynaGrid;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var <?= $generator->modelClass ?> $model
 */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;

$columns = [
<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 10) {
            echo "    [\n        '" . $name . "',\n    ],\n";
        } else {
            echo "    // [\n    //    '" . $name . "',\n    // ],\n";
        }
    }
} else {
    $columns = $generator->generateColumns();
    foreach ($columns as $column) {
        if (++$count < 10) {
            echo "    [\n";
            foreach ($column as $name => $field) {
                echo "        '{$name}' => {$field},\n";
            }
            echo "    ],\n";
        } else {
            echo "    // [\n";
            foreach ($column as $name => $field) {
                echo "    //     '{$name}' => {$field},\n";
            }
            echo "    // ],\n";
        }
    }
}
?>
];
?>

<?php echo "<?=" ?> DynaGrid::widget([
    'gridId' => 'dynagrid-<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>',
    'columns' => $columns,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
<?php if ($generator->getTableSchema()->getColumn('content_short') || $generator->getTableSchema()->getColumn('content_full') || $generator->getTableSchema()->getColumn('image_link')) : ?>
        'detailColumnEnabled' => true,
<?php else : ?>
        'detailColumnEnabled' => false,
<?php endif; ?>
<?php if ($generator->getTableSchema()->getColumn('status_key')) : ?>
        'toolbarApproveButtonEnabled' => true,
        'toolbarDisapproveButtonEnabled' => true,
        'actionApproveEnabled' => true,
        'actionDisapproveEnabled' => true,
<?php else : ?>
        'toolbarApproveButtonEnabled' => false,
        'toolbarDisapproveButtonEnabled' => false,
        'actionApproveEnabled' => false,
        'actionDisapproveEnabled' => false,
<?php endif; ?>
    ],
]) ?>
