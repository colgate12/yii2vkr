<?php
/**
 * This is the template for generating the model class of a specified table.
 */
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $generator common\gii\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */

$filterRules = $generator->generateFilterRules($tableSchema);

$imageLinkColumn = $tableSchema->getColumn('image_link');
$slugColumn = $tableSchema->getColumn('slug');

$keyColumns = $generator->getKeys($tableSchema);

echo "<?php\n";
?>

namespace <?= $generator->ns ?>;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * Модель таблицы "<?= $generator->generateTableName($tableName) ?>".
 *
<?php foreach ($tableSchema->columns as $column): ?>
 * @property <?= "{$column->phpType} \${$column->name} {$column->comment}\n" ?>
<?php endforeach; ?>
<?php if (!empty($keyColumns)) : ?>
 *
<?php foreach ($keyColumns as $keyColumnName => $keyColumn) : ?>
 * @property array <?= '$' . lcfirst(Inflector::pluralize($keyColumnName)) ?> <?= $keyColumn->comment ?> (Список)
 * @property string|null <?= '$' . lcfirst($keyColumnName) ?> <?= $keyColumn->comment . "\n" ?>
<?php endforeach; ?>
<?php endif; ?>
<?php if (!empty($relations)): ?>
 *
<?php foreach ($relations as $name => $relation): ?>
 * @property <?= $relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . "\n" ?>
<?php endforeach; ?>
<?php endif; ?>
 */
class <?= $className ?> extends <?= '\\' . ltrim($generator->baseClass, '\\') . "\n" ?>
{
<?php if ($imageLinkColumn) : ?>
    /** @var \yii\web\UploadedFile|string|null $file_image_link **/
    public $file_image_link;

<?php endif; ?>
    /**
     * @return string Название таблицы
     */
    public static function tableName()
    {
        return '<?= $generator->generateTableName($tableName) ?>';
    }
<?php if ($generator->db !== 'db'): ?>

    /**
     * @return \yii\db\Connection Соединение к базе
     */
    public static function getDb()
    {
        return Yii::$app->get('<?= $generator->db ?>');
    }
<?php endif; ?>

    /**
    * @return array Правила валидации
    */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            <?= implode(",\n            ", $rules) ?><?= ",\n" ?>
<?php if ($imageLinkColumn) : ?>
            [
                'file_image_link', 'image',
                'extensions' => 'png, jpg, jpeg, gif',
                'minWidth' => 50, 'maxWidth' => 3500,
                'minHeight' => 50, 'maxHeight' => 3500,
            ],
<?php endif; ?>
        ]);
    }

    /**
    * @return array Поведения
    */
    public function behaviors()
    {
        $items = parent::behaviors();

<?php if ($imageLinkColumn) : ?>
        $items['upload'] = [
            'class' => \common\behaviors\UploadBehavior::className(),
            'attributes' => ['image_link'],
            'scenarios' => ['insert', 'update', 'default'],
        ];

<?php endif; ?>
<?php if ($slugColumn) : ?>
        $items['slug'] = [
            'class' => \Zelenin\yii\behaviors\Slug::className(),
            'slugAttribute' => 'slug',
            'attribute' => 'name',
            'ensureUnique' => true,
            'replacement' => '-',
            'lowercase' => true,
            'immutable' => true,
        ];
<?php endif; ?>

        return $items;
    }

    /**
     * @return array Надписи атрибутов
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
    <?php foreach ($labels as $name => $label): ?>
        <?= "'$name' => " . $generator->generateString($label) . ",\n" ?>
<?php if ($name == 'image_link') : ?>
            <?= "'file_$name' => " . $generator->generateString($label) . ",\n" ?>
<?php endif; ?>
    <?php endforeach; ?>    ]);
    }
<?php if (!empty($filterRules)) : ?>

    /**
    * Attributes filter
    */
    protected function filterAttributes()
    {
<?php foreach ($filterRules as $filterRule) : ?>
        $this->_mainQuery->andFilterWhere(<?= $filterRule ?>);
<?php endforeach; ?>

        parent::filterAttributes();
    }
<?php endif; ?>
<?php if ($imageLinkColumn) : ?>
    /**
     * @param string $size
     * @param bool|false $isAbsolute
     * @return string
     */
    public function getImageUrl($size = 'original', $isAbsolute = false)
    {
        return $this->image_link ? \platx\filehelper\FileHelper::makeUrl($this->image_link, $size, $isAbsolute) : null;
    }

<?php endif; ?>
<?php if (!empty($keyColumns)) : ?>
<?php foreach ($keyColumns as $keyColumnName => $keyColumn) : ?>

    /**
    * <?= $keyColumn->comment ?> (Список)
    * @param null|string $key
    * @param null|mixed $defaultValue
    * @return array|mixed
    */
    public static function get<?= Inflector::pluralize($keyColumnName) ?>($key = null, $defaultValue = null)
    {
        $items = [

        ];

        return !is_null($key) ? ArrayHelper::getValue($items, $key, $defaultValue) : $items;
    }

    /**
    * <?= $keyColumn->comment . "\n" ?>
    * @return null|integer
    */
    public function get<?= $keyColumnName ?>()
    {
        return !is_null($this-><?= $keyColumn->name ?>) ? static::get<?= Inflector::pluralize($keyColumnName) ?>($this-><?= $keyColumn->name ?>) : null;
    }
<?php endforeach; ?>
<?php endif; ?>
<?php foreach ($relations as $name => $relation): ?>

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get<?= $name ?>()
    {
        <?= $relation[0] . "\n" ?>
    }
<?php endforeach; ?>
<?php if ($queryClassName): ?>
<?php
    $queryClassFullName = ($generator->ns === $generator->queryNs) ? $queryClassName : '\\' . $generator->queryNs . '\\' . $queryClassName;
    echo "\n";
?>
    /**
     * @inheritdoc
     * @return <?= $queryClassFullName ?> the active query used by this AR class.
     */
    public static function find()
    {
        return new <?= $queryClassFullName ?>(get_called_class());
    }
<?php endif; ?>
}
