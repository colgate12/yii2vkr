<?php
/**
 * This is the template for generating the model class of a specified table.
 */
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $generator common\gii\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */

$filterRules = $generator->generateFilterRules($tableSchema);

$imageLinkColumn = $tableSchema->getColumn('image_link');
$keyColumns = $generator->getKeys($tableSchema);

echo "<?php\n";
?>

namespace <?= $generator->consoleNs ?>;

use Yii;
use yii\helpers\ArrayHelper;


/**
* Модель таблицы "<?= $generator->generateTableName($tableName) ?>".
*
<?php foreach ($tableSchema->columns as $column): ?>
* @property <?= "{$column->phpType} \${$column->name} {$column->comment}\n" ?>
<?php endforeach; ?>
<?php if (!empty($keyColumns)) : ?>
*
<?php foreach ($keyColumns as $keyColumnName => $keyColumn) : ?>
* @property array <?= '$' . lcfirst(Inflector::pluralize($keyColumnName)) ?> <?= $keyColumn->comment ?> (Список)
* @property string|null <?= '$' . lcfirst($keyColumnName) ?> <?= $keyColumn->comment . "\n" ?>
<?php endforeach; ?>
<?php endif; ?>
<?php if (!empty($relations)): ?>
*
<?php foreach ($relations as $name => $relation): ?>
* @property <?= $relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . "\n" ?>
<?php endforeach; ?>
<?php endif; ?>
*/
class <?= $className ?> extends <?= '\\' . ltrim($generator->ns, '\\') . '\\' . $className . "\n" ?>
{
<?php foreach ($relations as $name => $relation): ?>
    /**
    * @return \yii\db\ActiveQuery
    */
    public function get<?= $name ?>()
    {
        <?= $relation[0] . "\n" ?>
    }
    
<?php endforeach; ?>
<?php if ($queryClassName): ?>
    <?php
    $queryClassFullName = ($generator->ns === $generator->queryNs) ? $queryClassName : '\\' . $generator->queryNs . '\\' . $queryClassName;
    echo "\n";
    ?>
    /**
    * @inheritdoc
    * @return <?= $queryClassFullName ?> the active query used by this AR class.
    */
    public static function find()
    {
        return new <?= $queryClassFullName ?>(get_called_class());
    }
<?php endif; ?>
}
