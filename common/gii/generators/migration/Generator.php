<?php

namespace common\gii\generators\migration;

use platx\filehelper\FileHelper;
use Yii;
use yii\db\Connection;
use yii\db\Migration;
use yii\gii\CodeFile;


/**
 * Class Generator
 * @package common\gii\generators\migration
 */
class Generator extends \yii\gii\Generator
{
    /** @var string $db */
    public $db = 'db';

    /** @var string $migrationPath */
    public $migrationPath = 'console/migrations';

    /** @var string $baseMigrationClass */
    public $baseClass = '\platx\db\Migration';

    /** @var bool $useTablePrefix */
    public $useTablePrefix = true;

    /** @var string $_tableName */
    private $_tableName;

    /** @var string $_migrationName */
    private $_migrationName;

    /** @var array $_columns */
    private $_columns;

    /** @var array $_foreignKeys */
    private $_foreignKeys;

    /** @var array $_indexes */
    private $_indexes;


    /**
     * @return string name of the code generator
     */
    public function getName()
    {
        return 'Генератор миграций';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'Генерирует миграцию с заданными параметрами.';
    }

    /**
     * Returns the message to be displayed when the newly generated code is saved successfully.
     * Child classes may override this method to customize the message.
     * @return string the message to be displayed when the newly generated code is saved successfully.
     */
    public function successMessage()
    {
        return 'Код успешно сгенерирован.';
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        return ['migration.php'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['tableName', 'required'],
            ['tableName', 'string', 'max' => 100, 'min' => 3],
            [['migrationName', 'migrationPath', 'baseClass'], 'string', 'max' => 255, 'min' => 3],
            [['tableName'], 'match', 'pattern' => '/^([\w_]+)$/', 'message' => 'Только буквенные символы и нижнее подчеркивание.'],
            [['migrationName'], 'match', 'pattern' => '/^([\w\d_]+)$/', 'message' => 'Только буквенные символы, цифры и нижнее подчеркивание.'],
            [['migrationPath'], 'match', 'pattern' => '/^([\w\d_\/]+)$/', 'message' => 'Только буквенные символы, цифры, нижнее подчеркивание и слеш.'],
            [['tableName', 'migrationName', 'migrationPath', 'baseClass'], 'filter', 'filter' => 'trim'],
            ['useTablePrefix', 'boolean'],
            [['db'], 'validateDb'],
            [['baseClass'], 'validateClass', 'params' => ['extends' => Migration::className()]],
            [['columns', 'foreignKeys', 'indexes'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'tableName' => 'Название таблицы',
            'migrationName' => 'Название миграции',
            'useTablePrefix' => 'Использовать префикс таблиц',
            'db' => 'Подключение к базе',
            'columns' => 'Свойства',
            'migrationPath' => 'Путь к папке миграций',
            'baseClass' => 'Базовый класс миграций',
            'foreignKeys' => 'Внешние ключи',
            'indexes' => 'Индексы',
        ];
    }

    /**
     * Validates the [[db]] attribute.
     */
    public function validateDb()
    {
        if (!Yii::$app->has($this->db)) {
            $this->addError('db', 'There is no application component named "db".');
        } elseif (!Yii::$app->get($this->db) instanceof Connection) {
            $this->addError('db', 'The "db" application component must be a DB connection instance.');
        }
    }

    /**
     * Generates the code based on the current user input and the specified code template files.
     * This is the main method that child classes should implement.
     * Please refer to [[\yii\gii\generators\controller\Generator::generate()]] as an example
     * on how to implement this method.
     * @return CodeFile[] a list of code files to be created.
     */
    public function generate()
    {
        $files = [];

        $migrationName = $this->getMigrationName();

        $params = [
            'tableName' => $this->generateTableName($this->getTableName()),
            'migrationName' => $migrationName,
            'db' => $this->db,
            'useTablePrefix' => $this->useTablePrefix,
            'baseClass' => $this->baseClass,
            'migrationPath' => $this->migrationPath,
            'columns' => $this->generateColumns(),
            'foreignKeys' => $this->generateForeignKeys(),
            'indexes' => $this->generateIndexes(),
        ];

        if ($this->getTableExists()) {
            $params['existColumns'] = $this->generateExistColumns();

            $template = 'update.php';
        } else {
            $template = 'create.php';
        }

        $files[] = new CodeFile(
            FileHelper::normalizePath(Yii::getAlias("@{$this->migrationPath}/{$migrationName}.php")),
            $this->render($template, $params)
        );

        return $files;
    }

    /**
     * @inheritdoc
     */
    public function autoCompleteData()
    {
        $db = $this->getDbConnection();
        if ($db !== null) {
            return [
                'tableName' => function () use ($db) {
                    return $db->getSchema()->getTableNames();
                },
            ];
        } else {
            return [];
        }
    }

    /**
     * @inheritdoc
     */
    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(), ['db', 'useTablePrefix']);
    }

    /**
     * Returns the `tablePrefix` property of the DB connection as specified
     *
     * @return string
     * @since 2.0.5
     * @see getDbConnection
     */
    public function getTablePrefix()
    {
        $db = $this->getDbConnection();
        if ($db !== null) {
            return $db->tablePrefix;
        } else {
            return '';
        }
    }

    /**
     * @return boolean the table names that match the pattern specified by [[tableName]].
     */
    protected function getTableExists()
    {
        $db = $this->getDbConnection();

        if ($db !== null && ($table = $db->getTableSchema($this->getTableName(), true)) !== null) {
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->_tableName;
    }

    /**
     * @param mixed $tableName
     */
    public function setTableName($tableName)
    {
        $this->_tableName = $tableName;
    }

    /**
     * @return mixed
     */
    public function getColumns()
    {
        return $this->_columns;
    }

    /**
     * @param mixed $newValue
     */
    public function setColumns($newValue)
    {
        $this->_columns = (array)$newValue;
    }

    public function generateColumns()
    {
        if (!$this->_columns) {
            return [];
        }

        $result = [];

        foreach ($this->_columns as $field) {
            if (!($field['name'])) {
                continue;
            }

            $result[$field['name']] = "\$this->";

            if (isset($field['type'])) {
                $result[$field['name']] .= $field['type'];
            }

            if (isset($field['length']) && $field['length']) {
                $result[$field['name']] .= '(' . $field['length'] . ')';
            } else {
                $result[$field['name']] .= '()';
            }

            if (isset($field['isNotNull']) && $field['isNotNull']) {
                $result[$field['name']] .= '->notNull()';
            }

            if (isset($field['isUnsigned']) && $field['isUnsigned']) {
                $result[$field['name']] .= '->unsigned()';
            }

            if (isset($field['isUnique']) && $field['isUnique']) {
                $result[$field['name']] .= '->unique()';
            }

            if (isset($field['default']) && $field['default'] !== '' && $field['default'] !== null) {
                $result[$field['name']] .= '->defaultValue(\'' . $field['default'] . '\')';
            }

            if (isset($field['comment']) && $field['comment']) {
                $result[$field['name']] .= '->comment(\'' . $field['comment'] . '\')';
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function generateExistColumns()
    {
        $columns = $this->getDbConnection()->getTableSchema($this->getTableName())->columns;

        $result = [];

        foreach ($columns as $name => $field) {
            $result[$name] = "\$this->";

            $result[$name] .= $field->type;

            if ($field->size) {
                $result[$name] .= '(' . $field->size . ')';
            } else {
                $result[$name] .= '()';
            }

            if (!$field->allowNull) {
                $result[$name] .= '->notNull()';
            }

            if ($field->unsigned) {
                $result[$name] .= '->unsigned()';
            }

            if ($field->defaultValue !== null) {
                $result[$name] .= '->defaultValue(\'' . $field->defaultValue . '\')';
            }

            if ($field->comment) {
                $result[$name] .= '->comment(\'' . $field->comment . '\')';
            }
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getForeignKeys()
    {
        return $this->_foreignKeys;
    }

    /**
     * @param mixed $newValue
     */
    public function setForeignKeys($newValue)
    {
        $this->_foreignKeys = (array)$newValue;
    }

    public function generateForeignKeys()
    {
        if (!$this->_foreignKeys) {
            return [];
        }

        $result = [];

        foreach ($this->_foreignKeys as $index => $field) {
            if (!$field['name']) {
                $field['name'] = $this->getTableName() . '_fk_' . $field['column'];
                $this->_foreignKeys[$index]['name'] = $field['name'];
            }

            $result[$field['name']] = "\$this->addForeignKey('{$field['name']}', '{$this->generateTableName($this->getTableName())}', '{$field['column']}', '{$this->generateTableName($field['refTable'])}', '{$field['refColumn']}', '{$field['onUpdate']}', '{$field['onDelete']}');";
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getIndexes()
    {
        return $this->_indexes;
    }

    /**
     * @param mixed $newValue
     */
    public function setIndexes($newValue)
    {
        $this->_indexes = (array)$newValue;
    }

    public function generateIndexes()
    {
        if (!$this->_indexes) {
            return [];
        }

        $result = [];

        foreach ($this->_indexes as $index => $field) {
            if (!is_array($field['columns'])) {
                $field['columns'] = explode(',', $field['columns']);

                foreach ($field['columns'] as $key => $column) {
                    $field['columns'][$key] = trim($column);
                }
            }

            if (!$field['name']) {
                $field['name'] = $this->getTableName() . '_i_' . implode('_', $field['columns']);
                $this->_indexes[$index]['name'] = $field['name'];
            }

            $field['columns'] = implode(', ', $field['columns']);

            $result[$field['name']] = "\$this->createIndex('{$field['name']}', '{$this->generateTableName($this->getTableName())}', '{$field['columns']}', {$field['isUnique']});";
        }

        return $result;
    }

    /**
     * @return \yii\db\Connection the DB connection as specified by [[db]].
     */
    protected function getDbConnection()
    {
        return Yii::$app->get($this->db, false);
    }

    /**
     * @return string
     */
    public function getMigrationName()
    {
        return $this->_migrationName;
    }

    /**
     * @param string $migrationName
     */
    public function setMigrationName($migrationName)
    {
        $this->_migrationName = $migrationName;

        $this->generateMigrationName();
    }

    /**
     * Generates the table name by considering table prefix.
     * If [[useTablePrefix]] is false, the table name will be returned without change.
     * @param string $tableName the table name (which may contain schema prefix)
     * @return string the generated table name
     */
    public function generateTableName($tableName)
    {
        if (!$this->useTablePrefix) {
            return $tableName;
        }

        $db = $this->getDbConnection();

        if (preg_match("/^{$db->tablePrefix}(.*?)$/", $tableName, $matches)) {
            $tableName = '{{%' . $matches[1] . '}}';
        } elseif (preg_match("/^(.*?){$db->tablePrefix}$/", $tableName, $matches)) {
            $tableName = '{{' . $matches[1] . '%}}';
        } else {
            $tableName = '{{%' . $tableName . '}}';
        }

        return $tableName;
    }

    public function generateMigrationName()
    {
        if (!$this->_migrationName) {
            if ($this->getTableExists()) {
                $type = 'update';
            } else {
                $type = 'create';
            }
            $this->_migrationName = $type . '_table_' . $this->getTableName();
        }

        if (!preg_match('/^m[\d]{6}_[\d]{6}_.*$/i', $this->_migrationName)) {
            $this->_migrationName = 'm' . date('ymd_His_') . $this->_migrationName;
        }
    }
}