<?php

/**
 * @var yii\web\View $this
 * @var common\gii\generators\migration\Generator $generator
 * @var string $tableName table name
 * @var string $migrationName name of current migration
 * @var string $baseClass base migration class name
 * @var array $columns columns list for migration
 * @var array $existColumns exist columns list of current table
 * @var array $foreignKeys foreign keys for current table
 * @var array $indexes indexes for current table
 */

echo "<?php\n";
?>

class <?= $migrationName ?> extends <?= $baseClass  . "\n"?>
{
    protected $_tableName = '<?= $tableName ?>';

    public function safeUp()
    {
<?php foreach ($columns as $name => $column) : ?>
<?php if (isset($existColumns[$name])) : ?>
        $this->alterColumn($this->_tableName, '<?= $name ?>', <?= $column ?>);
<?php else : ?>
        $this->addColumn($this->_tableName, '<?= $name ?>', <?= $column ?>);
<?php endif; ?>
<?php endforeach; ?>


<?php foreach ($foreignKeys as $foreignKey) : ?>
    <?= $foreignKey  . "\n" ?>
<?php endforeach; ?>
<?php foreach ($indexes as $index) : ?>
    <?= $index  . "\n" ?>
<?php endforeach; ?>
    }

    public function safeDown()
    {
<?php foreach ($columns as $name => $column) : ?>
<?php if (isset($existColumns[$name])) : ?>
        $this->alterColumn($this->_tableName, '<?= $name ?>', <?= $existColumns[$name] ?>);
<?php else : ?>
        $this->dropColumn($this->_tableName, '<?= $name ?>');
<?php endif; ?>
<?php endforeach; ?>

<?php foreach ($foreignKeys as $name => $foreignKey) : ?>
    $this->dropForeignKey('<?= $name ?>', $this->_tableName);
<?php endforeach; ?>
<?php foreach ($indexes as $name => $index) : ?>
    $this->dropIndex('<?= $name ?>', $this->_tableName);
<?php endforeach; ?>
    }
}
