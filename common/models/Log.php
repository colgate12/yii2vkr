<?php

namespace common\models;

use common\db\ActiveRecord;
use common\helpers\Param;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 * Логирование
 *
 * @property integer $id
 * @property integer $record_id Запись
 * @property integer $user_id Пользователь
 * @property string $entity Сущность
 * @property string $comment Комментарий
 * @property string $data Данные
 * @property array $dataArray Данные в виде массива
 * @property string $ip IP адрес
 * @property string $type_key Ключ типа
 * @property string $type Название типа
 * @property string $level_key Ключ уровня
 * @property string $level Название уровня
 * @property string $entityClass Класс сущности
 * @property string $entityLabel Название сущности
 * @property string $created_at Дата создания
 *
 * @property ActiveRecord $record Запись
 * @property User $user Пользователь
 */
class Log extends ActiveRecord
{
    const TYPE_NONE = 'none';

    const ENTITY_NONE = 'none';

    const LEVEL_NONE = 'none';
    const LEVEL_ERROR = 'error';
    const LEVEL_SUCCESS = 'success';
    const LEVEL_WARNING = 'warning';
    const LEVEL_INFO = 'info';
    const LEVEL_TRACE = 'trace';

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%log}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['comment', 'type_key'], 'required'],
            [['comment', 'entity'], 'string', 'max' => 255],
            [['type_key', 'level_key'], 'string', 'max' => 20],
            [['data'], 'string'],
            [['ip'], 'string', 'max' => 50],
            [['record_id', 'user_id'], 'integer'],
            ['level_key', 'default', 'value' => self::LEVEL_NONE],
            ['type_key', 'default', 'value' => self::TYPE_NONE],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     *
     */
    public function afterValidate()
    {
        if (!$this->checkLevel($this->level_key)) {
            $this->addError('level_key', "Уровень {$this->level} не разрешен!");
        }

        parent::afterValidate();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'user_id' => 'Пользователь',
            'record_id' => 'Запись',
            'entity' => 'Сущность',
            'comment' => 'Комментарий',
            'data' => 'Данные',
            'dataArray' => 'Данные',
            'ip' => 'IP адрес',
            'type_key' => 'Тип',
            'type' => 'Тип',
            'level_key' => 'Уровень',
            'level' => 'Уровень',
        ]);
    }

    /**
     * Фильтрация по атрибутам
     */
    public function filterAttributes()
    {
        parent::filterAttributes();

        $this->_mainQuery->andFilterWhere(['=', 'entity', $this->entity]);
        $this->_mainQuery->andFilterWhere(['=', 'record_id', $this->record_id]);
        $this->_mainQuery->andFilterWhere(['=', 'user_id', $this->user_id]);
        $this->_mainQuery->andFilterWhere(['=', 'type_key', $this->type_key]);
        $this->_mainQuery->andFilterWhere(['like', 'comment', $this->comment]);
        $this->_mainQuery->andFilterWhere(['like', 'ip', $this->ip]);
        $this->_mainQuery->andFilterWhere(['=', 'level_key', $this->level_key]);
    }

    /**
     * @param null $key
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public static function getTypes($key = null, $defaultValue = null)
    {
        $items = [
            static::TYPE_NONE => 'Нет',
        ];

        if (!is_null($key)) {
            return ArrayHelper::getValue($items, $key, $defaultValue);
        }

        return $items;
    }

    /**
     * @param null $defaultValue
     *
     * @return array|mixed|null
     */
    public function getType($defaultValue = null)
    {
        return !is_null($this->type_key) ? $this->getTypes($this->type_key) : $defaultValue;
    }

    /**
     * Список уровней сообщений
     * @param null $key
     * @return array|mixed
     */
    public static function getLevels($key = null)
    {
        $items = [
            self::LEVEL_NONE => 'Не указан',
            self::LEVEL_ERROR => 'Ошибка',
            self::LEVEL_SUCCESS => 'Успех',
            self::LEVEL_WARNING => 'Предупреждение',
            self::LEVEL_INFO => 'Информация',
            self::LEVEL_TRACE => 'Трассировка',
        ];

        return !is_null($key) ? ArrayHelper::getValue($items, $key) : $items;
    }

    /**
     * Название уровня сообщения
     * @return array|mixed
     */
    public function getLevel()
    {
        return self::getLevels($this->level_key);
    }

    /**
     * @param string $column
     * @param null $key
     *
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public static function getEntities($column = 'name', $key = null, $defaultValue = null)
    {
        $items = [
            static::ENTITY_NONE => [
                'name' => 'Нет',
                'class' => $defaultValue,
            ],
        ];

        if (!is_null($key)) {
            return ArrayHelper::getValue(ArrayHelper::getColumn($items, $column), $key, $defaultValue);
        }

        return ArrayHelper::getColumn($items, $column);
    }

    /**
     * @return array|mixed|null
     */
    public function getEntityLabel()
    {
        return !is_null($this->entity) ? static::getEntities('name', $this->entity) : null;
    }

    /**
     * @return array|mixed|null
     */
    public function getEntityClass()
    {
        return !is_null($this->entity) ? static::getEntities('class', $this->entity) : null;
    }

    /**
     * @return \yii\db\ActiveQuery Запись
     */
    public function getRecord()
    {
        return $this->getEntityClass() ? $this->hasOne($this->getEntityClass(), ['id' => 'record_id']) : null;
    }

    /**
     * @return \yii\db\ActiveQuery Запись
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param $newValue
     *
     * @return $this
     */
    public function setDataArray($newValue)
    {
        $this->setData($newValue);

        return $this;
    }

    /**
     * @param null $key
     * @param null $defaultValue
     *
     * @return array|mixed|string
     */
    public function getDataArray($key = null, $defaultValue = null)
    {
        $data = $this->data ? is_string($this->data) ? Json::decode($this->data) : $this->data : [];

        return !is_null($key) ? ArrayHelper::getValue($data, $key, $defaultValue) : $data;
    }

    /**
     * @param $key
     * @param $newValue
     *
     * @return $this
     */
    public function setDataItem($key, $newValue)
    {
        $data = $this->getDataArray();

        $data[$key] = $newValue;

        $this->setDataArray($data);

        return $this;
    }

    /**
     * @param $newValue
     *
     * @return $this
     */
    public function setComment($newValue)
    {
        $this->comment = (string)$newValue;

        return $this;
    }

    /**
     * @param $newValue
     *
     * @return $this
     */
    public function setLevel($newValue)
    {
        $this->level_key = (int)$newValue;

        return $this;
    }

    /**
     * @param $newValue
     *
     * @return $this
     */
    public function setData($newValue)
    {
        $this->data = is_array($newValue) ? Json::encode($newValue) : $newValue;

        return $this;
    }

    /**
     * @param $newValue
     *
     * @return $this
     */
    public function setType($newValue)
    {
        $this->type_key = (string)$newValue;

        return $this;
    }

    /**
     * @param $newValue
     *
     * @return $this
     */
    public function setEntity($newValue)
    {
        $this->entity = (string)$newValue;

        return $this;
    }

    /**
     * @param $newValue
     *
     * @return $this
     */
    public function setRecord($newValue)
    {
        $this->record_id = ($newValue instanceof ActiveRecord) ? $newValue->id : (int)$newValue;

        return $this;
    }

    /**
     * @param User|int $newValue
     *
     * @return $this
     */
    public function setUser($newValue)
    {
        $this->user_id = ($newValue instanceof User) ? $newValue->id : (int)$newValue;

        return $this;
    }

    /**
     * @param $newValue
     *
     * @return $this
     */
    public function setIp($newValue)
    {
        $this->ip = (string)$newValue;

        return $this;
    }

    /**
     * @return array
     */
    protected function getContext()
    {
        $context = [];
        $logVars = Param::get('log.vars', []);

        foreach ($logVars as $name) {
            if (!empty($GLOBALS[$name])) {
                $context[$name] = $GLOBALS[$name];
            }
        }

        return $context;
    }

    /**
     * @param $level_key
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    protected function checkLevel($level_key)
    {
        return in_array(Param::get('log.levels'), $level_key);
    }

    /**
     * @param array $params
     *
     * @return $this
     */
    public static function add($params = [])
    {
        $model = new static;

        $model->setAttributes($params);

        $dataArray = ArrayHelper::merge($model->getDataArray(null, []), $model->getContext());

        $model->setDataArray($dataArray);

        return $model;
    }

    /**
     * @param array $params
     *
     * @return Log
     */
    public static function error($params = [])
    {
        $params['level_key'] = static::LEVEL_ERROR;

        return static::add($params);
    }

    /**
     * @param array $params
     *
     * @return Log
     */
    public static function success($params = [])
    {
        $params['level_key'] = static::LEVEL_SUCCESS;

        return static::add($params);
    }

    /**
     * @param array $params
     *
     * @return Log
     */
    public static function warning($params = [])
    {
        $params['level_key'] = static::LEVEL_WARNING;

        return static::add($params);
    }

    /**
     * @param array $params
     *
     * @return Log
     */
    public static function info($params = [])
    {
        $params['level_key'] = static::LEVEL_INFO;

        return static::add($params);
    }

    /**
     * @param array $params
     *
     * @return Log
     */
    public static function trace($params = [])
    {
        $params['level_key'] = static::LEVEL_TRACE;

        return static::add($params);
    }
}