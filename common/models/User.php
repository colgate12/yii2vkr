<?php

namespace common\models;

use common\behaviors\UploadBehavior;
use common\db\ActiveRecord;
use common\helpers\FileHelper;
use Yii;
use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;


/**
 * Пользователи
 *
 * @property integer $id
 * @property integer $network_key
 * @property string $network_id
 * @property string $email
 * @property string $phone
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $image_link
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $status_key
 * @property string $ip
 * @property string $created_at
 * @property string $updated_at
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * Константы статусов
     */
    const STATUS_MODERATION = 0;
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    /**
     * Константы социальных сетей
     */
    const NETWORK_SITE = 0;
    const NETWORK_VK = 1;
    const NETWORK_FB = 2;
    const NETWORK_OK = 3;
    const NETWORK_TW = 4;
    const NETWORK_IG = 5;

    /**
     * @var
     */
    public $password;
    public $password_confirm;

    /**
     * @var
     */
    public $file_image_link;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['first_name', 'last_name', 'middle_name', 'phone'], 'string', 'max' => 100],
            [['first_name', 'last_name', 'email'], 'required', 'on' => 'default'],
            [['password', 'password_confirm'],'required', 'on' => 'insert'],
            [['auth_key', 'ip'], 'string', 'max' => 50],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот E-mail уже зарегистрирован.'],
            ['password', 'string', 'min' => 5, 'message' => 'Пароль должен быть не короче 5 символов'],
            ['password_confirm', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли должны совпадать'],
            [
                'file_image_link', 'image',
                'extensions' => 'png, jpg, jpeg, gif',
                'minWidth' => 50, 'maxWidth' => 3500,
                'minHeight' => 50, 'maxHeight' => 3500,
            ],
            ['network_key', 'integer'],
            ['network_id', 'string', 'max' => '255'],
            ['network_key', 'required', 'on' => 'social'],
            ['network_id', 'required', 'on' => 'social'],
        ]);
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        $items = parent::scenarios();

        $items['social'] = ['first_name', 'last_name', 'middle_name', 'email', 'phone', 'image_link', 'network_key', 'network_id'];
        $items['update'] = ['first_name', 'last_name', 'middle_name', 'email', 'phone', 'image_link', 'network_key', 'network_id', 'file_image_link', 'password', 'password_confirm'];

        return $items;
    }

    /**
     * @return array Поведения
     */
    public function behaviors()
    {
        $items = parent::behaviors();

        $items['upload'] = [
            'class' => UploadBehavior::className(),
            'attributes' => ['image_link'],
            'scenarios' => ['insert', 'update', 'default'],
        ];

        return $items;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'email' => 'E-mail',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'phone' => 'Телефон',
            'password' => 'Пароль',
            'password_confirm' => 'Повторить пароль',
            'network_id' => 'ID в соц. сети',
            'network_key' => 'Авторизация',
            'ip' => 'IP адрес',
            'image_link' => 'Фото',
            'file_image_link' => 'Картинка',
            'status_key' => 'Статус',
            'status' => 'Статус',
        ]);
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function beforeSave($insert) {
        if(isset($this->password) && !empty($this->password)) {
            $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        }

        if(!empty(Yii::$app->request)) {
            $this->ip = Yii::$app->request->userIP;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by email
     *
     * @param integer $networkKey
     * @param string $networkId
     * @return static|null
     */
    public static function findByNetwork($networkKey, $networkId)
    {
        return static::findOne([
            'network_key' => $networkKey,
            'network_id' => $networkId,
            'status_key' => self::STATUS_ENABLED
        ]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function filterAttributes()
    {
        parent::filterAttributes();

        $this->_mainQuery->andFilterWhere(['like', 'first_name', $this->first_name]);
        $this->_mainQuery->andFilterWhere(['like', 'last_name', $this->last_name]);
        $this->_mainQuery->andFilterWhere(['like', 'email', $this->email]);
        $this->_mainQuery->andFilterWhere(['like', 'phone', $this->phone]);
        $this->_mainQuery->andFilterWhere(['like', 'network_id', $this->network_id]);
        $this->_mainQuery->andFilterWhere(['=', 'network_key', $this->network_key]);
    }
    
    /**
     * Список статусов
     *
     * @param null $key
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public static function getStatuses($key = null, $defaultValue = null)
    {
        $items = [
            self::STATUS_MODERATION => 'На проверке',
            self::STATUS_ENABLED => 'Активен',
            self::STATUS_DISABLED => 'Забанен',
        ];

        return !is_null($key) ? ArrayHelper::getValue($items, $key, $defaultValue) : $items;
    }

    /**
     * Название статуса
     *
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public function getStatus($defaultValue = null)
    {
        return static::getStatuses($this->status_key, $defaultValue);
    }

    /**
     * Список соц. сетей
     * @param null $key
     * @return array|mixed
     */
    public static function getNetworks($key = null)
    {
        $items = [
            self::NETWORK_SITE => 'Сайт',
            self::NETWORK_VK => 'Вконтакте',
            self::NETWORK_FB => 'Facebook',
            self::NETWORK_OK => 'Одноклассники',
            self::NETWORK_TW => 'Twitter',
            self::NETWORK_IG => 'Instagram',
        ];

        return !is_null($key) ? ArrayHelper::getValue($items, $key) : $items;
    }

    /**
     * Название соц. сети
     * @return array|mixed
     */
    public function getNetwork()
    {
        return self::getNetworks($this->network_key);
    }

    /**
     * @param string $size
     * @param bool|false $isAbsolute
     * @return string
     */
    public function getImageUrl($size = 'original', $isAbsolute = false)
    {
        return $this->image_link ? FileHelper::makeUrl($this->image_link, $size, $isAbsolute) : null;
    }

    /**
     * @return null|string
     */
    public function getNetworkUrl()
    {
        switch($this->network_key) {
            case self::NETWORK_VK :
                return "http://vk.com/id{$this->network_id}";
                break;
            case self::NETWORK_FB :
                return "http://facebook.com/{$this->network_id}";
                break;
            case self::NETWORK_OK :
                return "http://ok.ru/profile/{$this->network_id}";
                break;
            case self::NETWORK_TW :
                return "http://twitter.com/intent/user?user_id={$this->network_id}";
                break;
            default :
                return null;
        }
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->status_key == static::STATUS_ENABLED;
    }
}
