<?php

namespace common\db;

use common\behaviors\SortableGridBehavior;
use common\helpers\DateHelper;
use yii\db\ActiveRecord as YiiActiveRecord;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;


/**
 * Class ActiveRecord
 *
 * @property integer $id
 * @property integer $status_key
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 * @property string $updated
 * @property string $created
 *
 * @package common\db
 */
abstract class ActiveRecord extends YiiActiveRecord
{
    /**
     * Константы статусов
     */
    const STATUS_MODERATION = 0;
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    public $createdAtStart;
    public $createdAtEnd;

    public $updatedAtStart;
    public $updatedAtEnd;

    /**
     * Query builder object
     * @var null|ActiveQuery
     */
    protected $_mainQuery;

    /**
     * Old attributes
     * @var array
     */
    protected $_oldAttributes;

    /**
     * @return array
     */
    public function rules()
    {
        $items = parent::rules();

        if ($this->hasAttribute('created_at')) {
            $items[] = ['createdAtStart', 'safe'];
            $items[] = ['createdAtEnd', 'safe'];
        }

        if ($this->hasAttribute('updated_at')) {
            $items[] = ['updatedAtStart', 'safe'];
            $items[] = ['updatedAtEnd', 'safe'];
        }

        if ($this->hasAttribute('position')) {
            $items[] = ['position', 'integer'];
        }

        if ($this->hasAttribute('status_key')) {
            $items[] = ['status_key', 'integer'];
            $items[] = ['status_key', 'in', 'range' => array_keys(static::getStatuses())];
        }

        return $items;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $items = [];

        if ($this->hasProperty('created_at')) {
            $items['date'] = [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => $this->hasProperty('updated_at') ? 'updated_at' : 'created_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ];
        }

        if($this->hasProperty('position')) {
            $items['sort'] = [
                'class' => SortableGridBehavior::class,
                'sortableAttribute' => 'position'
            ];
        }

        return $items;
    }

    /**
     * Set old attributes
     */
    public function afterFind()
    {
        $this->_oldAttributes = $this->attributes;

        parent::afterFind();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $items = parent::attributeLabels();

        if($this->hasAttribute('id')) {
            $items['id'] = '#';
        }
        if($this->hasAttribute('status_key')) {
            $items['status_key'] = 'Статус';
            $items['status'] = 'Статус';
        }
        if($this->hasAttribute('position')) {
            $items['position'] = 'Позиция';
        }
        if($this->hasAttribute('created_at')) {
            $items['created_at'] = 'Дата создания';
            $items['created'] = 'Дата создания';
        }
        if ($this->hasAttribute('updated_at')) {
            $items['updated_at'] = 'Дата обновления';
            $items['updated'] = 'Дата обновления';
        }

        return $items;
    }

    /**
     * Class short name
     * @param bool|true $toLower
     * @param bool|true $pluralize
     * @return mixed|string
     */
    public static function getClassNameShort($toLower = false, $pluralize = false)
    {
        $fullClassName = self::class;

        $explodedClassName = explode('\\', $fullClassName);
        $classNameShort = end($explodedClassName);

        if ($toLower) {
            return mb_strtolower($classNameShort);
        }

        return !$pluralize ? $classNameShort : Inflector::pluralize($classNameShort);
    }

    /**
     * Search model by id
     * @param $id
     * @param null $where
     * @return mixed
     */
    public static function findByPk($id, $where = null)
    {
        /** @var static $model */
        $query = static::find()->andWhere(['id' => $id]);

        if (is_array($where)) {
            $query->andWhere($where);
        }

        return $query->one();
    }

    /**
     * Data for DropDownList, RadioList, CheckboxList
     * Returns data array or empty array
     * @param string $indexField Field for array keys
     * @param string $labelField Field for array values
     * @param array $where Filtering data
     * @return array Data array
     */
    public static function listAll($indexField = 'id', $labelField = 'name', $where = null)
    {
        $query = static::find();

        if (is_array($where)) {
            $query->andWhere($where);
        }

        $items = ArrayHelper::map($query->all(), $indexField, $labelField);

        return $items;
    }

    /**
     * Check updated attribute
     * @param string $attribute Attribute name
     * @return bool Result
     */
    public function isChanged($attribute) {
        if(isset($this->_oldAttributes[$attribute]) && isset($this->{$attribute})) {
            return $this->_oldAttributes[$attribute] != $this->{$attribute};
        }

        return false;
    }

    /**
     * Search models
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->_mainQuery = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $this->_mainQuery,
        ]);

        if($this->hasAttribute('position')) {
            $dataProvider->sort = [
                'defaultOrder' => [
                    'position' => SORT_ASC,
                ],
            ];
        } else if($this->hasAttribute('created_at')) {
            $dataProvider->sort = [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ];
        }

        if(!empty($params[$this->getClassNameShort()])) {
            $this->load($params);
        } else {
            $this->setAttributes($params);
        }

        $this->filterAttributes();

        return $dataProvider;
    }

    /**
     * Build query with filter attributes
     * @return null|ActiveQuery Query builder
     */
    public function buildQuery()
    {
        $this->_mainQuery = self::find();

        $this->filterAttributes();

        return $this->_mainQuery;
    }

    /**
     * Фильтрация по атрибутам
     */
    protected function filterAttributes()
    {
        if($this->_mainQuery) {
            if($this->hasAttribute('status_key')) {
                $this->_mainQuery->andFilterWhere(['=', 'status_key', $this->status_key]);
            }
            if($this->hasAttribute('id')) {
                $this->_mainQuery->andFilterWhere(['id' => $this->id]);
            }
            if($this->hasAttribute('created_at')) {
                $this->_mainQuery->andFilterWhere(['like', 'created_at', $this->created_at]);
            }
            if($this->hasAttribute('updated_at')) {
                $this->_mainQuery->andFilterWhere(['like', 'updated_at', $this->updated_at]);
            }
        }
    }

    /**
     * Список статусов
     *
     * @param null $key
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public static function getStatuses($key = null, $defaultValue = null)
    {
        $items = [
            self::STATUS_ENABLED => 'Опубликовано',
            self::STATUS_DISABLED => 'Отклонено',
            self::STATUS_MODERATION => 'На модерации',
        ];

        return !is_null($key) ? ArrayHelper::getValue($items, $key, $defaultValue) : $items;
    }

    /**
     * Название статуса
     *
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public function getStatus($defaultValue = null)
    {
        return !is_null($this->status_key) ? static::getStatuses($this->status_key, $defaultValue) : $defaultValue;
    }

    /**
     * Дата создания
     *
     * @param string $format
     * @param bool $plural
     * @param null $defaultValue
     *
     * @return string
     */
    public function getCreated($format = 'd M Y', $plural = true, $defaultValue = null)
    {
        if (empty($this->created_at)) {
            return $defaultValue;
        }

        return DateHelper::dateFormatted($this->created_at, $format, $plural);
    }

    /**
     * Дата обновления
     *
     * @param string $format
     * @param bool $plural
     * @param null $defaultValue
     *
     * @return string
     */
    public function getUpdated($format = 'd M Y', $plural = true, $defaultValue = null)
    {
        if (empty($this->updated_at)) {
            return $defaultValue;
        }

        return DateHelper::dateFormatted($this->updated_at, $format, $plural);
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function fields()
    {
        $items = parent::fields();

        $excludeFields = $this->excludeFields();

        if (!is_array($excludeFields)) {
            throw new InvalidConfigException(get_class($this) . '::excludeFields() must be an array.');
        }

        foreach ($excludeFields as $excludeField) {
            unset($items[$excludeField]);
        }

        return $items;
    }

    /**
     * @return array
     */
    public function excludeFields()
    {
        return [];
    }

    /**
     * @param bool $runValidation
     *
     * @return bool
     */
    public function approve($runValidation = true)
    {
        $this->status_key = static::STATUS_ENABLED;

        return $this->save($runValidation);
    }

    /**
     * @param bool $runValidation
     *
     * @return bool
     */
    public function disapprove($runValidation = true)
    {
        $this->status_key = static::STATUS_DISABLED;

        return $this->save($runValidation);
    }

    /**
     * Duplicate entries in the table.
     * @return $this|null
     */
    public function duplicate() {
        $this->isNewRecord = true;

        foreach ($this->primaryKey() as $key) {
            $this->{$key} = null;
        }
        if ($this->save()) {
            return $this;
        }

        return null;
    }

    /**
     * @param string $condition
     * @param array $params
     *
     * @return int
     * @throws \yii\db\Exception
     */
    public static function approveAll($condition = '', $params = [])
    {
        $command = static::getDb()->createCommand();
        $command->update(static::tableName(), ['status_key' => static::STATUS_ENABLED], $condition, $params);

        return $command->execute();
    }

    /**
     * @param string $condition
     * @param array $params
     *
     * @return int
     * @throws \yii\db\Exception
     */
    public static function disapproveAll($condition = '', $params = [])
    {
        $command = static::getDb()->createCommand();
        $command->update(static::tableName(), ['status_key' => static::STATUS_DISABLED], $condition, $params);

        return $command->execute();
    }

    /**
     * @inheritdoc
     * @return ActiveQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }
}