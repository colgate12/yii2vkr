<?php

namespace common\db;

use yii\db\ActiveQuery as YiiActiveQuery;


/**
 * Class ActiveQuery
 * @package common\db
 */
class ActiveQuery extends YiiActiveQuery
{
    /**
     * @param int $key
     *
     * @return $this
     */
    public function status($key = ActiveRecord::STATUS_ENABLED) {
        return $this->andWhere(['status_key' => $key]);
    }

    /**
     * @inheritdoc
     * @return \common\db\ActiveRecord[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\db\ActiveRecord|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}