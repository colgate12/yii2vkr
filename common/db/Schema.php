<?php

namespace common\db;

use yii\db\mysql\Schema as YiiSchema;


/**
 * Class Schema
 * @package common\db
 */
class Schema extends YiiSchema
{
    const TYPE_CHAR = 'char';

    const CASCADE = 'CASCADE';
    const RESTRICT = 'RESTRICT';
}