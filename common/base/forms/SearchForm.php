<?php

namespace common\base\forms;

use common\db\ActiveRecord;
use platx\httperror\HttpError;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\Query;


/**
 * Class SearchForm
 * @package common\base\forms
 */
abstract class SearchForm extends Model
{
    /** @var  Query */
    protected $mainQuery;

    /**
     * @var string название модели для поиска
     */
    public $modelClass = null;

    /**
     * параметры поиска
     * @var null
     */
    public $limit = 10;
    public $offset = 0;

    /**
     * @throws \yii\web\HttpException
     */
    public function init()
    {
        if ($this->modelClass === null) {
            HttpError::the500('Please, set "modelClass" property in your child form');
        }

        parent::init();
    }

    /**
     * @return array
     */
    public function selectAttributes()
    {
        return [
            'id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['offset', 'limit'], 'integer'],
            ['modelClass', 'string', 'max' => 255],
            ['selectAttributes', 'safe'],
        ];
    }

    /**
     * Построение запроса
     * @return \yii\db\ActiveQuery
     */
    public function buildQuery()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;

        /** @var ActiveQuery $mainQuery */
        $this->mainQuery = $model::find();
        $this->mainQuery->andWhere(['status_key' => $model::STATUS_ENABLED]);

        if (!empty($this->selectAttributes())) {
            $this->mainQuery->select($this->selectAttributes());
        }

        $this->filterAttributes();

        $this->mainQuery->limit($this->limit);
        $this->mainQuery->offset($this->offset);

        return $this->mainQuery;
    }

    /**
     *
     */
    protected function filterAttributes()
    {

    }
}