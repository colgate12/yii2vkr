<?php

namespace common\helpers;

use yii\base\Component;


/**
 * Class SystemHelper
 * @package common\helpers
 */
class SystemHelper extends Component
{
    /** @var array  */
    protected static $_stat = [];

    /**
     * @param bool $force
     *
     * @return mixed|null
     */
    public static function getCpuUsage($force = false)
    {
        if(is_null(ArrayHelper::getValue(self::$_stat, 'cpu.usage')) || $force) {
            $load = sys_getloadavg();
            self::$_stat['cpu']['usage'] = ArrayHelper::getValue($load, 0);
        }

        return ArrayHelper::getValue(self::$_stat, 'cpu.usage');
    }

    /**
     * @param bool $force
     *
     * @return mixed|null
     */
    public static function getMemoryUsage($force = false)
    {
        if(is_null(ArrayHelper::getValue(self::$_stat, 'memory.usage')) || $force) {
            $free = shell_exec('free');
            $free = (string)trim($free);
            $free_arr = explode("\n", $free);
            $mem = explode(" ", $free_arr[1]);
            $mem = array_filter($mem);
            $mem = array_merge($mem);
            $memory_usage = $mem[2]/$mem[1]*100;

            self::$_stat['memory']['usage'] = $memory_usage;
        }

        return ArrayHelper::getValue(self::$_stat, 'memory.usage');
    }

    /**
     * @param bool $force
     *
     * @return mixed|null
     */
    public static function getHddUsage($force = false)
    {
        if(is_null(ArrayHelper::getValue(self::$_stat, 'hdd.usage')) || $force) {
            $stat['hdd_free'] = round(disk_free_space("/") / 1024 / 1024 / 1024, 2);
            $stat['hdd_total'] = round(disk_total_space("/") / 1024 / 1024/ 1024, 2);
            $stat['hdd_used'] = $stat['hdd_total'] - $stat['hdd_free'];
            $stat['hdd_percent'] = round(sprintf('%.2f',($stat['hdd_used'] / $stat['hdd_total']) * 100), 2);


            self::$_stat['hdd']['usage'] = $stat['hdd_percent'];
        }

        return ArrayHelper::getValue(self::$_stat, 'hdd.usage');
    }

    /**
     * @param bool $force
     *
     * @return mixed|null
     */
    public static function getUpTime($force = false)
    {
        if(is_null(ArrayHelper::getValue(self::$_stat, 'up.time')) || $force) {
            $ut = strtok(@exec("cat /proc/uptime"), ".");
            $days = sprintf("%2d", ($ut / (3600 * 24)));
            $hours = sprintf("%2d", (($ut % (3600 * 24))) / 3600);
            $min = sprintf("%2d", ($ut % (3600 * 24) % 3600) / 60);
            $sec = sprintf("%2d", ($ut % (3600 * 24) % 3600) % 60);
            $upTime = array($days, $hours, $min, $sec);


            self::$_stat['up']['time'] = $upTime;
        }

        return ArrayHelper::getValue(self::$_stat, 'up.time');
    }
}