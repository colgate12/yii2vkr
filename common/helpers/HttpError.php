<?php

namespace common\helpers;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;

/**
 * HttpError Helper for Yii2
 */
class HttpError
{
    /**
     * Causes Bad request error - Code 400
     *
     * @param string $message Message for displaying in stack trace
     * @param int    $code
     *
     * @throws BadRequestHttpException
     */
    public static function the400($message = 'Bad request', $code = 0)
    {
        throw new BadRequestHttpException($message, $code);
    }

    /**
     * Causes Authorization error - Code 401
     *
     * @param string $message Message for displaying in stack trace
     * @param int    $code
     *
     * @throws UnauthorizedHttpException
     */
    public static function the401($message = 'Not authorized', $code = 0)
    {
        throw new UnauthorizedHttpException($message, $code);
    }

    /**
     * Causes not found error - Code 404
     *
     * @param string $message Message for displaying in stack trace
     * @param int    $code
     *
     * @throws NotFoundHttpException
     */
    public static function the404($message = 'Not found', $code = 0)
    {
        throw new NotFoundHttpException($message, $code);
    }

    /**
     * Causes Forbidden error - Code 403
     *
     * @param string $message Message for displaying in stack trace
     * @param int    $code
     *
     * @throws ForbiddenHttpException
     */
    public static function the403($message = 'Forbidden', $code = 0)
    {
        throw new ForbiddenHttpException($message, $code);
    }

    /**
     * Causes internal server error - Code 500
     *
     * @param string $message Message for displaying in stack trace
     * @param int    $code
     *
     * @throws HttpException
     */
    public static function the500($message = 'Server error', $code = 0)
    {
        throw new HttpException(500, $message, $code);
    }

    /**
     * Causes validation error
     *
     * @param array $params Additional parameters
     * @return array Response array
     */
    public static function validateError($params = [])
    {
        Yii::$app->response->setStatusCode(422);

        return $params;
    }
}
