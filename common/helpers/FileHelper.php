<?php

namespace common\helpers;

use yii\helpers\Url;


/**
 * Extended FileHelper for Yii2
 * @package platx\filehelper
 */
class FileHelper extends \yii\helpers\FileHelper
{
	/** @var string Web folder */
	public static $alias = '@frontend/web/';

	/** @var string Uploads folder */
	public static $uploadDir = '/uploads/';

	/**
	 * Generates path with model id
	 * @param int $id ID in DB
	 * @return string Path
	 */
	public static function dynamicPath($id)
	{
		$n = floor($id);
		$c = floor($n/125000000);
		$a = floor((($n - $c * 125000000)/250000));
		$b = floor((($n - $a * 250000 - $c * 125000000)/500));

		return self::normalizePath("$c/$a/$b");
	}

	/**
	 * Returns file url for requested size.
	 * @param            $path - path to file from DB
	 * @param string     $size - image size
	 * @param bool|false $absoluteUrl
	 * @return string
	 */
	public static function makeUrl($path, $size = 'original', $absoluteUrl = false) {
		if (preg_match('/^(http:\/\/|https:\/\/|\/\/).*/', $path)) {
			return $path;
		}

		return Url::to(self::normalizePath(self::$uploadDir . $size . $path, '/'), $absoluteUrl);
	}


	/**
	 * Generates file path for save in DB
	 * @param string $model_name Model short name
	 * @param string $field Field for saving path
	 * @param integer $id Model ID
	 * @param null|string $file_name File name
	 * @param null|string $size Size
	 * @return string Url
	 */
	public static function fileUrl($model_name, $field, $id, $file_name = null, $size = null) {
		$dp = self::dynamicPath($id);
		$model_name = self::camelCaseToUnderscore($model_name);
		$path = "/$model_name/$field/$dp";
		$path = $size ? "/$size$path" : $path;

		return self::normalizePath($path . ($file_name ? ('/' . $file_name) : ''), '/');
	}

	/**
	 * Generates full path of file on the server
	 * @param string $model_name Model short name
	 * @param string $field Field for saving file path
	 * @param integer $id Model ID
	 * @param string $size Size
	 * @param string $file_name File name
	 * @return string Path
	 */
	public static function filePath($model_name, $field, $id, $size = 'original', $file_name = null) {
		$folder = self::fileUrl($model_name, $field, $id, $file_name, $size);

		return self::normalizePath(\Yii::getAlias(self::$alias . self::$uploadDir . $folder));
	}

	/**
	 * Camel case to underscore string
	 * @param string $string Camel case string
	 * @return string Underscore string
	 */
	public static function camelCaseToUnderscore($string) {
		preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
		$ret = $matches[0];
		return strtolower(implode('_', $ret));
	}
}
