<?php

namespace common\helpers;

use yii\base\BaseObject;


/**
 * Class WordHelper
 * @package common\helpers
 */
class WordHelper extends BaseObject
{
    /**
     * Преобразование числа
     * @param $n
     * @param $forms
     * @return mixed
     */
    public static function plural($n, $forms)
    {
        return $n % 10 == 1 && $n % 100 != 11 ? $forms[0] : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $forms[1] : $forms[2]);
    }

    /**
     * @param $str
     * @return string
     */
    public static function translit($str)
    {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '',  'ы' => 'y',   'ъ' => '',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',

        );

        return strtr($str, $converter);
    }

    /**
     * обрезание текста по заданной длине
     * с учетом целостности слов
     * @param $string
     * @param int $length
     * @return string
     */
    public static function shortText($string, $length = 9)
    {
        $string = strip_tags($string);
        $arr_str = explode(" ", $string);
        $arr = array_slice($arr_str, 0, $length);
        $new_str = implode(" ", $arr);
        if (count($arr_str) > $length) {
            $new_str .= '...';
        }
        return $new_str;
    }

	public static function rjust($string, $total_length, $fillchar=' ')
	{
		// if the string is longer than the total length allowed just return it
		if(strlen($string) >= $total_length)
		{
			return $string;
		}

		$total_length = intval($total_length);

		// total_length must be a number greater than 0
		if( ! $total_length )
		{
			return $string;
		}

		// the $fillchar can't be empty
		if(!strlen($fillchar))
		{
			return $string;
		}

		// make the fill character into padding
		while(strlen($fillchar) < $total_length)
		{
			$fillchar = $fillchar.$fillchar;
		}


		return substr($fillchar.$string, ( -1 * $total_length ));

	}

	public static function wordWrap($string, $width = 75, $break = ' ') {
		return preg_replace('/(\S{' . $width . '})/u', '$1' . $break, $string);
	}

	public static function camelCaseToUnderscore($string) {
		preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
		$ret = $matches[0];
		return strtolower(implode('_', $ret));
	}

}