<?php

namespace common\helpers;

use DateTime;
use yii\base\BaseObject;


/**
 * Class DateHelper
 * @package common\helpers
 */
class DateHelper extends BaseObject
{
    /**
     * Возвращает список дней недели
     * @param null $key
     * @param bool $plural
     * @return array
     */
    public static function getMonthTitle($key = null, $plural = false)
    {
        $items = [
            1 => [
                'Январь',
                'Января'
            ],
            2 => [
                'Февраль',
                'Февраля'
            ],
            3 => [
                'Март',
                'Марта'
            ],
            4 => [
                'Апрель',
                'Апреля'
            ],
            5 => [
                'Май',
                'Мая'
            ],
            6 => [
                'Июнь',
                'Июня'
            ],
            7 => [
                'Июль',
                'Июля'
            ],
            8 => [
                'Август',
                'Августа'
            ],
            9 => [
                'Сентябрь',
                'Сентября'
            ],
            10 => [
                'Октябрь',
                'Октября'
            ],
            11 => [
                'Ноябрь',
                'Ноября'
            ],
            12 => [
                'Декабрь',
                'Декабря'
            ],
        ];

        if (!is_null($key) && isset($items[$key])) {
            return $items[$key][$plural];
        }

        return $items;
    }

    /**
     * Возвращает список дней недели
     * @param null $key
     * @return array
     */
    public static function getDayTitle($key = null)
    {
        $items = [
            0 => 'Воскресенье',
            1 => 'Понедельник',
            2 => 'Вторник',
            3 => 'Среда',
            4 => 'Четверг',
            5 => 'Пятница',
            6 => 'Суббота',
        ];

        if (!is_null($key) && isset($items[$key])) {
            return $items[$key];
        }

        return $items;
    }

    /**
     * Возвращает название дня недели
     * @param $date
     * @return string
     */
    public static function dateDayTitle($date)
    {
        if (!$date instanceof DateTime) {
            $date = new DateTime($date);
        }

        return static::getDayTitle($date->format('w'));
    }

    /**
     * Возвращает название месяца
     * @param $date
     * @param bool $plural
     * @return string
     */
    public static function dateMonthTitle($date, $plural = true)
    {
        if (!$date instanceof DateTime) {
            $date = new DateTime($date);
        }

        return static::getMonthTitle($date->format('n'), $plural);
    }

    /**
     * Геттер отформатированной даты
     * @param $date
     * @param string $format
     * @param bool $plural
     * @return string
     */
    public static function dateFormatted($date, $format = 'd-m-Y', $plural = false)
    {
        if (!$date instanceof DateTime) {
            $date = new DateTime($date);
        }

        if (strpos($format, 'M') !== false) {
            $monthTitle = static::getMonthTitle($date->format('n'), $plural);
            $format = str_replace('M', $monthTitle, $format);
        }

        return $date->format($format);
    }
}