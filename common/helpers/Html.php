<?php

namespace common\helpers;

use Yii;
use yii\helpers\Html as YiiHtml;


/**
 * Class Html
 * @package common\helpers
 */
class Html extends YiiHtml
{
    /**
     * @param string $content the content to be enclosed between the start and end td tags. It will not be HTML-encoded.
     * If this is coming from end users, you should consider [[encode()]] it to prevent XSS attacks.
     * @param array $options the tag options in terms of name-value pairs. These will be rendered as
     * the attributes of the td tag. The values will be HTML-encoded using [[encode()]].
     * If a value is null, the corresponding attribute will not be rendered.
     *
     * - head: boolean, whether this row is table header. Defaults to false.
     *
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     *
     * @return string the generated table column.
     */
    public static function td($content, $options = [])
    {
        $head = ArrayHelper::remove($options, 'head', false);

        return !$head ? self::tag('td', $content, $options) : self::th($content, $options);
    }

    /**
     * @param string $content the content to be enclosed between the start and end th tags. It will not be HTML-encoded.
     * If this is coming from end users, you should consider [[encode()]] it to prevent XSS attacks.
     * @param array $options the tag options in terms of name-value pairs. These will be rendered as
     * the attributes of the th tag. The values will be HTML-encoded using [[encode()]].
     * If a value is null, the corresponding attribute will not be rendered.
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     *
     * @return string the generated table head column.
     */
    public static function th($content, $options = [])
    {
        return self::tag('th', $content, $options);
    }

    /**
     * @param array $items the items for generating table row. Each item generates a single table column.
     * Note that items will be automatically HTML encoded if `$options['encode']` is not set or true.
     * @param array $options options (name => config) for the table row. The following options are supported:
     *
     * - head: boolean, whether this row is table header. Defaults to false.
     * - colOptions: array, the HTML attributes for the `td` or `th` tags.
     *
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     *
     * @return string the generated table row. An empty tr tag will be returned if `$items` is empty.
     */
    public static function tr($items, $options = [])
    {
        $colOptions = ArrayHelper::remove($options, 'colOptions', []);
        $colOptions['head'] = ArrayHelper::remove($options, 'head', false);

        $html = self::beginTag('tr', $options);

        foreach ($items as $item) {
            if (is_array($item)) {
                $html .= self::td(
                    ArrayHelper::remove($item, 'value', ''),
                    ArrayHelper::merge(
                        $colOptions,
                        ArrayHelper::remove($item, 'options', [])
                    )
                );
            } else if (strpos($item, 'td')) {
                $html .= $item;
            } else {
                $html .= self::td(
                    $item,
                    $colOptions
                );
            }
        }

        $html .= self::endTag('tr');

        return $html;
    }

    /**
     * @param array $items the items for generating header rows. Each item generates a single row.
     * Note that items will be automatically HTML encoded if `$options['encode']` is not set or true.
     * @param array $options options (name => config) for the thead tag. The following options are supported:
     *
     * - rowOptions: array, the HTML attributes for the `tr` tags.
     * - colOptions: array, the HTML attributes for the `td` tags.
     *
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     *
     * @return string the generated header for table. An empty thead tag will be returned if `$items` is empty.
     */
    public static function thead($items, $options = [])
    {
        $rowOptions = ArrayHelper::remove($options, 'rowOptions', []);
        $rowOptions['colOptions'] = ArrayHelper::remove($options, 'colOptions', []);
        $rowOptions['head'] = true;

        $html = self::beginTag('thead', $options);

        foreach ($items as $item) {
            if (is_array($item)) {
                $html .= self::tr($item, $rowOptions);
            } else {
                $html .= $item;
            }
        }

        $html .= self::endTag('thead');

        return $html;
    }

    /**
     * @param array $items the items for generating table rows. Each item generates a single row.
     * Note that items will be automatically HTML encoded if `$options['encode']` is not set or true.
     * @param array $options options (name => config) for the table rows. The following options are supported:
     *
     * - rowOptions: array, the HTML attributes for the `tr` tags.
     * - colOptions: array, the HTML attributes for the `td` tags.
     *
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     *
     * @return string the generated body for table. An empty tbody tag will be returned if `$items` is empty.
     */
    public static function tbody($items, $options = [])
    {
        $rowOptions = ArrayHelper::remove($options, 'rowOptions', []);
        $rowOptions['colOptions'] = ArrayHelper::remove($options, 'colOptions', []);

        $html = self::beginTag('tbody', $options);

        foreach ($items as $item) {
            if (is_array($item)) {
                $html .= self::tr($item, $rowOptions);
            } else {
                $html .= $item;
            }
        }

        $html .= self::endTag('tbody');

        return $html;
    }

    /**
     * @param array $items the items for generating table footer rows. Each item generates a single row.
     * Note that items will be automatically HTML encoded if `$options['encode']` is not set or true.
     * @param array $options options (name => config) for the tfoot tag. The following options are supported:
     *
     * - rowOptions: array, the HTML attributes for the `tr` tags.
     * - colOptions: array, the HTML attributes for the `td` tags.
     *
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     *
     * @return string the generated footer for table. An empty tfoot tag will be returned if `$items` is empty.
     */
    public static function tfoot($items, $options = [])
    {
        $rowOptions = ArrayHelper::remove($options, 'rowOptions', []);
        $rowOptions['colOptions'] = ArrayHelper::remove($options, 'colOptions', []);

        $html = self::beginTag('tfoot', $options);

        foreach ($items as $item) {
            if (is_array($item)) {
                $html .= self::tr($item, $rowOptions);
            } else {
                $html .= $item;
            }
        }

        $html .= self::endTag('tfoot');

        return $html;
    }

    /**
     * @param array $items the items for generating the table. Each item generates a single row.
     * Note that items will be automatically HTML encoded if `$options['encode']` is not set or true.
     * @param array $options options (name => config) for the table. The following options are supported:
     *
     * - rowOptions: array, the HTML attributes for the `tr` tags.
     * - colOptions: array, the HTML attributes for the `td` tags.
     *
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     *
     * @return string the generated table. An empty table tag will be returned if `$items` is empty.
     */
    public static function table($items, $options = [])
    {
        $rowOptions = ArrayHelper::remove($options, 'rowOptions', []);
        $rowOptions['colOptions'] = ArrayHelper::remove($options, 'colOptions', []);

        $html = self::beginTag('table', $options);

        foreach ($items as $item) {
            if (is_array($item)) {
                $html .= self::tr($item, $rowOptions);
            } else {
                $html .= $item;
            }
        }

        $html .= self::endTag('table');

        return $html;
    }

    /**
     * @param array $items the items for generating html code. Each item generates a single html tag.
     * Note that items will be automatically HTML encoded if `$options['encode']` is not set or true.
     *
     * Each item in `$items` array can be an array (child items), string (tag value) or the following options:
     * - tag: string, a name of current tag.
     * - options: array, the HTML attributes for current tag.
     *
     * $items example you can see in the following code:
     *
     * ```php
     * $content = Html::tags([
     *      'html' => [
     *          'options' => ['lang' => 'en'],
     *          'head' => [
     *              'title' => 'Hello world',
     *              Html::cssFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'),
     *          ],
     *          'body' => [
     *              [
     *                  'tag' => 'div',
     *                  'options' => ['style' => 'text-align: center;'],
     *                  'h1' => 'Hello world!'
     *              ],
     *              [
     *                  'tag' => 'hr'
     *              ]
     *          ]
     *      ],
     * ]);
     * ```
     *
     * @param array $cache options for caching generated html:
     * - enabled: boolean, whether generated html must be cached. Defaults to false.
     * - key: string, a key identifying the cached value.
     *
     *
     * @return string the generated html. An empty string will be returned if `$items` is empty.
     */
    public static function tags($items, $cache = [])
    {
        $cachedData = false;
        $cacheEnabled = ArrayHelper::remove($cache, 'enabled', false);
        $cacheKey = ArrayHelper::remove($cache, 'key', 'tags-' . count($items));

        if ($cacheEnabled && $cachedData = Yii::$app->cache->get($cacheKey)) {
            return $cachedData;
        }

        $html = '';

        if (is_array($items) && !empty($items)) {
            foreach ($items as $tag => $item) {
                if (!is_string($tag)) {
                    $tag = ArrayHelper::remove($item, 'tag', null);
                }

                $options = ArrayHelper::remove($item, 'options', []);

                $html .= self::beginTag($tag, $options);

                if (is_array($item)) {
                    $html .= self::tags($item);
                } else {
                    $html .= $item;
                }

                $html .= self::endTag($tag);
            }
        }

        if ($cacheEnabled && !$cachedData) {
            Yii::$app->cache->set($cacheKey, $html);
        }

        return $html;
    }
}