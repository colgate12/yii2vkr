<?php

namespace common\behaviors;

use common\db\ActiveRecord;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\Expression;


/**
 * Class SortableGridBehavior
 * @package common\behaviors
 */
class SortableGridBehavior extends \himiklab\sortablegrid\SortableGridBehavior
{
    /**
     * @var bool
     */
    public $reverse = false;

    /**
     * @var string
     */
    public $sortableAttribute = 'position';

    /**
     * @throws InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function beforeInsert()
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        if (!$model->hasAttribute($this->sortableAttribute)) {
            throw new InvalidConfigException("Invalid sortable attribute `{$this->sortableAttribute}`.");
        }

        if($this->reverse) {
            $model->{$this->sortableAttribute} = 1;
            Yii::$app->db->createCommand()->update($model::tableName(), ['position' => new Expression("position + 1")])->execute();
        } else {
            $query = $model::find();
            if (is_callable($this->scope)) {
                call_user_func($this->scope, $query);
            }

            $maxOrder = $query->max($model::tableName() . '.' . $this->sortableAttribute);
            $model->{$this->sortableAttribute} = $maxOrder + 1;
        }
    }
}