<?php

namespace common\widgets;

use common\helpers\Html;
use common\helpers\HttpError;

/**
 * Class NewDynaGrid
 *
 * @package common\widgets
 */
class DynaGrid extends \kartik\dynagrid\DynaGrid
{
    public $storage = self::TYPE_COOKIE;

    public $title;

    public $theme = 'panel-default';

    public $gridId;

    public $exportConfig;

    public $isSortable;

    public $columns;

    /** @throws \yii\web\HttpException */
    public function init()
    {
        if (empty($this->gridId)) {
            HttpError::the500('Необходимо указать уникальный ID DynaGrid\'а');
        }

        $this->options['id'] = $this->gridId;

        if (empty($this->title)) {
            $this->title = 'Все записи';
        }

        $this->gridOptions['panel']['heading'] = Html::tag('h1', $this->title, ['class' => 'panel-title']);
        $this->gridOptions['options']['id']    = $this->gridId . '-table';

        parent::init();
    }

    /**
     * @return string|void
     * @throws \Exception
     */
    public function run()
    {
        echo $this->isSortable ? SortableGridView::widget($this->gridOptions) : GridView::widget($this->gridOptions);
    }
}