<?php

namespace common\widgets;

use kartik\dynagrid\Module;

class DynaGridModule extends Module
{
    public $configView = '@vendor/kartik-v/yii2-dynagrid/views/config';
    public $settingsView = '@vendor/kartik-v/yii2-dynagrid/views/settings';
}