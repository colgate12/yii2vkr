<?php

namespace common\widgets\assets;

use yii\web\AssetBundle;


/**
 * Class FroalaEditorAsset
 * @package common\widgets\assets
 */
class FroalaEditorAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/assets';
    public $js = [
        'js/froala_editor.js',
    ];
    public $css = [
        'css/froala_editor.css',
    ];
}