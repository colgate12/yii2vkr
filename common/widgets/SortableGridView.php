<?php

namespace common\widgets;

use himiklab\sortablegrid\SortableGridAsset;
use yii\helpers\Url;


/**
 * Class SortableGridView
 * @package common\widgets
 */
class SortableGridView extends GridView
{
    /** @var string|array Sort action */
    public $sortableAction = ['sort'];
    public function init()
    {
        parent::init();
        $this->sortableAction = Url::to($this->sortableAction);
    }
    public function run()
    {
        $this->registerWidget();
        parent::run();
    }
    protected function registerWidget()
    {
        $view = $this->getView();
        $view->registerJs("jQuery('#{$this->options['id']}').SortableGridView('{$this->sortableAction}');");
        SortableGridAsset::register($view);
    }
}