<?php

use yii\helpers\Html;
use yii\helpers\Url;


/**
 * @var $this yii\web\View
 * @var $model common\models\User
 */

$resetLink = Url::to(['/auth/password-reset', 'token' => $model->password_reset_token]);

?>

Здравствуйте, <?= Html::encode($model->email) ?> !

Перейдите по ссылке ниже для восстановления Вашего пароля:

<?= $resetLink ?>
