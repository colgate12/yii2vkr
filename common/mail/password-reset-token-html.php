<?php

use yii\helpers\Html;
use yii\helpers\Url;


/**
 * @var $this yii\web\View
 * @var $model common\models\User
 */

$resetLink = Url::to(['/auth/password-reset', 'token' => $model->password_reset_token]);

?>

<div class="password-reset">
    <p>Здравствуйте, <?= Html::encode($model->email) ?> !</p>

    <p>Перейдите по ссылке ниже для восстановления Вашего пароля:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
